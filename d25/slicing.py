# We start at 3 and skip every 2nd from the end if we use double colon

name = "Dominika"
print(name[3::2])
output = "iaa"


# Here we have single colon!
print(name[1:6:2])




