# id() is used to display the location number of the memory in RAM
print(id("A"))

# ord() function is used to get the ordinal value.
print(ord("A"))

# binary ordinal value
print(bin(ord("A")))

greetings = "Hello"
print("location number of memory: ", end="")
print(id(greetings))


ram_locations = []
for character in greetings:
    ram_locations.append(ord(character))

# Adding value 0 at the end of the string for the CPU to recognise it
ram_locations.append(0)
print(ram_locations)