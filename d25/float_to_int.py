"""
Note that the int function converts a float to an int by truncation, \
    not by rounding to the nearest whole number. Truncation simply chops off the number’s fractional part.\
        The round function rounds a float to the nearest int as in the next example: >>> int(6.75) 6
"""

print(round(6.78))
number = 5.78

print(int(number)) # Conversion from float to int data type makes the fractional part being cut off

# print(help(round))


