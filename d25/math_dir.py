import math # math module

from math import * # to import everything

"""

To use a resource from a module, you write the name of a module as a qualifier, \
    followed by a dot (.) and the name of the resource.
    
    For example, to use the value of pi from the math module, you would write the following code: math.pi.
"""

# print(dir(math))

print(math.sqrt(2))

# print(help(math.cos))

"""
If you are going to use only a couple of a module’s resources frequently, \
    you can avoid the use of the qualifier with each reference by importing the individual resources, \
        as follows: >>> from math import pi, sqrt

"""

print(pow(2, 3))


# Test with a reasonable set of legitimate inputs