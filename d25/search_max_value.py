import pandas

data = pandas.read_csv("weather_data.csv")
data_converted = data["temp"].convert_dtypes(convert_integer=True)
max_value = data_converted[0]
# print(data_converted)

# The shortest solution:    
print(data_converted.max())

# DataFrame = data
# Series = data["temp"]

# data.temp is accessible as an attribute
# data["temp"] you are treating it more like a dictionary with the key called ["temp"]
# data.temp    you are treating it more like an object and .temp is similar to an attribute

# The longer way:
for number in data_converted:
    if number > number - 1:
        maximum_value = number
print(maximum_value)    
