import pandas

"""_summary_

Using .loc and lambda enables us to chain data selection operations without using a temporary variable 
and helps prevent errors.



Let’s say we have an error in the age variable. We recorded ages with a difference of 3 years. 
So, to remove this error from the Pandas dataframe, we have to add three years to every person’s age.
We can do this with the apply() function in Pandas.

apply() function calls the lambda function and applies it to every row or column of the dataframe and 
returns a modified copy of the dataframe:

We can use the apply() function to apply the lambda function to both rows and columns of a dataframe. 
If the axis argument in the apply() function is 0, then the lambda function gets applied to each column, and 
if 1, then the function gets applied to each row.

apply() function can also be applied directly to a Pandas series:

"""


values= [['Rohan',455],['Elvish',250],['Deepak',495],['Soni',400],['Radhika',350],['Vansh',450]]


data_frame = pandas.DataFrame(values, columns=["Name", "Total_Marks"])

# data_frame = data_frame.assign(Percentage = lambda x: (x["Total_Marks"]/ 500 * 100))

data_frame = data_frame.assign(Percentage = lambda x: (x.Total_Marks / 500 * 100))

print(data_frame)
print("\n\n\n")

data_frame = data_frame.assign(Percentage = data_frame.Total_Marks / 500 * 100)



print(data_frame)