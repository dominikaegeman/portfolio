import random
from turtle import Turtle, Screen

screen = Screen()
screen.setup(600, 700)


def creating_turtle():
    current_y = [-150, -100, -50, 0, 50, 100]
    colours = ["red", "purple", "green", "blue", "yellow", "orange"]
    all_turtles = []
    for turtle_index in range(0, 6):
        new_turtle = Turtle(shape="turtle")
        another_colour = colours[turtle_index]
        new_turtle.color(another_colour)
        new_turtle.penup()
        new_turtle.goto(-280, current_y[turtle_index])
        all_turtles.append(new_turtle)

    return all_turtles


def user_bet():
    user_bet = screen.textinput("Make a bet", "Who is going to win the race? ")
    return user_bet


def random_walk():

    turtles_all = creating_turtle()
    race_on = False
    user_answer = user_bet()
    if user_answer:
        race_on = True
    while race_on:
        for turtle in turtles_all:
            random_distance = random.randint(1, 20)
            turtle.forward(random_distance)
            if turtle.xcor() > 280:
                race_on = False
                winning_turtle = turtle.pencolor()
                if turtle.pencolor() == user_answer:
                    print(f"You win, the {winning_turtle} turtle won the race! ")
                else:
                    print(f"You lost, the {winning_turtle} turtle won the race! ")


random_walk()
screen.exitonclick()
