import turtle

tim = turtle.Turtle()
screen = turtle.Screen()


def move_forward():
    tim.forward(10)


def move_backward():
    tim.backward(10)


def changing_angle_clockwise():
    # tim.right(10)
    new_heading = tim.heading() - 10
    tim.setheading(new_heading)


def changing_angle_counter_clockwise():
    # tim.left(10)
    new_heading = tim.heading() + 10
    tim.setheading(new_heading)


def clearing():
    tim.clear()
    tim.penup()
    tim.home()  # we will go back to the origin position
    tim.pendown()


# screen.clear() will delete everything even the tim
# it can be good to just use tim.reset()

def moving():
    screen.listen()
    screen.onkey(key="w", fun=move_forward)
    screen.onkey(key="s", fun=move_backward)
    screen.onkey(key="d", fun=changing_angle_clockwise)
    screen.onkey(key="a", fun=changing_angle_counter_clockwise)
    screen.onkey(key="c", fun=clearing)


moving()
screen.exitonclick()
# when we use a function as an argument so sth that is going be passed in to another function, \
# we do not add the parenthesis at the end
