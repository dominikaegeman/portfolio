from turtle import Turtle, Screen
import random

is_race_on = False

screen = Screen()
screen.setup(width=500, height=400)
# We have used keywords arguments instead of positional arguments, because we now know which argument is which
# Positional is when the position of the argument decides about it
# Keyword is when the keyword decides about the argument

user_bet = screen.textinput(title="Make your bet", prompt="Which title will win the race? Enter a colour: ")
colours = ["red", "blue", "green", "yellow", "orange", "purple"]

y_positions = [-70, -40, - 10, 20, 50, 80]

all_turtles = []

for turtle_index in range(0, 6):
    new_turtle = Turtle(shape="turtle")
    new_turtle.penup()
    new_turtle.color(colours[turtle_index])
    new_turtle.goto(x=-230, y=y_positions[turtle_index])
    all_turtles.append(new_turtle)


if user_bet:
    is_race_on = True

while is_race_on:

    for turtle in all_turtles:
        if turtle.xcor() > 230:
            is_race_on = False
            winning_turtle = turtle.pencolor()
            # turtle.color() is showing us pencolor and fillcolor, so better to use e.g. .pencolor()
            if winning_turtle == user_bet:
                print(f"You won. The {winning_turtle} turtle is the winner! ")
            else:
                print(f"You have lost. The {winning_turtle} turtle is the winner! ")
    for turtle in all_turtles:
        random_distance = random.randint(0, 10)
        turtle.forward(random_distance)

screen.exitonclick()

# tim = Turtle()
# tim.hideturtle()
# tim.shape("turtle")
# tim.color(colours[0])
# tim.penup()
# tim.goto(x = -230, y = 120)
# tim.showturtle()
#
# tommy = Turtle()
# tommy.hideturtle()
# tommy.shape("turtle")
# tommy.color(colours[1])
# tommy.penup()
# tommy.goto(x = -230, y = 80)
# tommy.showturtle()
#
# timmy = Turtle()
# timmy.hideturtle()
# timmy.shape("turtle")
# timmy.color(colours[2])
# timmy.penup()
# timmy.goto(x = -230, y = 40)
# timmy.showturtle()
#
# alice = Turtle()
# alice.hideturtle()
# alice.shape("turtle")
# alice.color(colours[3])
# alice.penup()
# alice.goto(x = -230, y = -40)
# alice.showturtle()
#
# tuptup = Turtle()
# tuptup.hideturtle()
# tuptup.shape("turtle")
# tuptup.color(colours[4])
# tuptup.penup()
# tuptup.goto(x = -230, y = -80)
# tuptup.showturtle()
#
# bubu = Turtle()
# bubu.hideturtle()
# bubu.shape("turtle")
# bubu.color(colours[5])
# bubu.penup()
# bubu.goto(x = -230, y = -120)
# bubu.showturtle()

# final_list_turtles = []
#
#
# def creating_objects(first_part_name, second_part_name):
#     """ Creating objects """
#     list_of_names = [first_part_name, second_part_name]
#     turtle = "".join(list_of_names)
#
#     global final_list_turtles
#     final_list_turtles.append(turtle)
#
#
#
# turtles = {"tim =": " Turtle()", "tommy =": " Turtle()", "bubu =": " Turtle()", "timmy =": " Turtle()", "alice =": " Turtle()", "tuptup =": " Turtle()"}
#
#
# def intro_settings(list_of_objects):
#     """ The objects will be associated with an initial settings """
#     list_of_objects[0].penup()
#     list_of_objects[0].goto(x = -230, y = -100)
#
#     print(list_of_objects)
#
#
# for key, value in turtles.items():
#     creating_objects(key, value)
#
# intro_settings(final_list_turtles)

# name =[]
# name.append(Turtle(shape="circle"))
# name.append(Turtle(shape="turtle"))
#
# name[0].penup()
# name[0].goto(-190, -100)
