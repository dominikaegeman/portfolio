def add(n1, n2):
    return n1 + n2


def subtraction(n1, n2):
    return n1-n2


def multiply(n1, n2):
    return n1 * n2


def divide(n1, n2):
    return n1/n2


def calculator(n1, n2, function):
    return function(n1, n2)


result = calculator(2, 3, multiply)
print(result)

"""
Higher Order Function is a function that can work with other function
Calculator is a Higher Order Function because it is taking another function as an input and working with it 


"""