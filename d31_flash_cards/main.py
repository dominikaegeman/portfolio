import tkinter
import os
import pandas 
import random
BACKGROUND_COLOR = "#B1DDC6"

try:
    df = pandas.read_csv(f"{os.getcwd()}/d31_flash_cards/data/words_to_learn.csv")
    
except FileNotFoundError:
    df = pandas.read_csv(f"{os.getcwd()}/d31_flash_cards/data/danish_words.csv")



list_of_danish_words = df.to_dict(orient="records")
current_card = {}

def remove_card():
    list_of_danish_words.remove(current_card)

    
    words_to_learn = pandas.DataFrame(list_of_danish_words)
    words_to_learn.to_csv(f"{os.getcwd()}/d31_flash_cards/data/words_to_learn.csv", index=False)
    

def flip_card():
    main_canvas.itemconfig(canvas_photo, image=back_card_image)
    main_canvas.itemconfig(language_chosen, fill="white", text="English")
    main_canvas.itemconfig(current_word, text=f'{current_card["English"]}', fill="white")



def choosing_word(card_to_remove_or_leave):
    global current_card, flip_timer
    window.after_cancel(flip_timer)
    
    random_row = random.choice(list_of_danish_words)
    random_danish_word = (random_row["Danish"])
    main_canvas.itemconfig(language_chosen, text="Danish", fill="black")
    main_canvas.itemconfig(current_word, text=random_danish_word, fill="black")
    
    current_card = {}
    current_card = random_row
    
    main_canvas.itemconfig(canvas_photo, image=front_card_image)
    flip_timer = window.after(3000, flip_card)

    
    if card_to_remove_or_leave == "remove":
        remove_card()





window = tkinter.Tk()
window.title("Flash Cards")
#window.geometry('1000x800')
window.config(padx=50, pady=50, bg=BACKGROUND_COLOR)

flip_timer = window.after(3000, flip_card) 


main_canvas = tkinter.Canvas(width=900, height=586, bg=BACKGROUND_COLOR, highlightthickness=0)
front_card_image = tkinter.PhotoImage(file=f"{os.getcwd()}/d31_flash_cards/images/card_front.png")
back_card_image = tkinter.PhotoImage(file=f"{os.getcwd()}/d31_flash_cards/images/card_back.png")
canvas_photo = main_canvas.create_image(450, 280, image=front_card_image) # 450, 250 to be in center of Canvas()
#IMPORTANT: PhotoImage objects should not be created inside a function. Otherwise, it will not work.


main_canvas.grid(row=0, column=0,columnspan=2)

language_chosen = main_canvas.create_text(450, 150, text="Title", fill="black", font=("Ariel", 40, "italic")) # 400, 150 in the middle and on the top of Canvas()
current_word = main_canvas.create_text(450, 250, text="Word", fill="black", font=("Ariel", 60, "bold") )


wrong_image = tkinter.PhotoImage(file=f"{os.getcwd()}/d31_flash_cards/images/wrong.png")
wrong_button = tkinter.Button(image=wrong_image, highlightthickness=0, command=lambda: choosing_word("leave"))
wrong_button.grid(row=1, column=0)

right_image = tkinter.PhotoImage(file=f"{os.getcwd()}//d31_flash_cards/images/right.png")
right_button = tkinter.Button(image=right_image, highlightthickness=0, command=lambda:choosing_word("remove"))
right_button.grid(row=1, column=1)



choosing_word("start") #It will allow to display "Danish" and the random word automatically once we run the app





window.mainloop()
