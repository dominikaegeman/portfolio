'''
Type Hint:
Keep code safer
'''


age: int
name: str
height: float
is_guman: bool



def police_check(age:int) -> bool:
    if age > 18:
        can_drive = True
    else:
        can_drive = False
    
    return can_drive


print(police_check(12))

if police_check(19):
    print("You may pass")
    
    