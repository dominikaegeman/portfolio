class User:
    def __init__(self):
        print("This will be printed every time the new object is added")


user_1 = User()
user_1.id = "001"
user_1.username = "Name"

# print(user_1.username)


user_2 = User()
user_2.id = "002"

"""
Every letter of the Class Name shall be a capital letter e.g. class NewCar

PascalCase            | Class
camelCase             | Only few situations when the camelCase is used
snake_case            | Pretty everything else

Attribute is a variable that is associated with an object
E.g. user_1.id presented below
"""


"""
We can introduce
user_2 = User()
user_2.id = "002"
user_2.username = "Second User"

"""

# Initializing an object = we can set variables or counters to their starting values
# To initialize it is good to create a Constructor

# Constructor


"""
def __init__(self):
    #initialise attributes

"""
