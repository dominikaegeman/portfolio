
# A method unlike a function always needs to have a self parameter!
# When this method is called, it knows the object that called it


class User:

    def __init__(self, user_id, username):
        self.id = user_id
        self.username = username
        self.followers = 0
        self.following = 0

    def follow(self, user_followed):
        user_followed.followers += 1
        self.following += 1


user_1 = User("001", "Name1")
user_2 = User("002", "Name2")


user_1.follow(user_2)
print(user_1.followers)
print(user_1.following)
print(user_2.followers)
print(user_2.following)
