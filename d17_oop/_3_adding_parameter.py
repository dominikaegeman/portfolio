
from test import ciam


class User:
    def __init__(self, user_id, username):
        self.id = user_id
        self.username = username
        self.followers = 0

# The name of the parameter is generally the name of the attribute
# user_id | parameter
# .id     | attribute


user_1 = User("001", "Name_1")
user_2 = User("002", "Name_2")

print(user_1.followers)

# Constructors are generally used for instantiating an object. CONSTRUCTOR IS THE INIT FUNCTION
# The task of constructors is to initialize(assign values) to the data members of the class
# when an object of the class is created. In Python the __init__() method is called the constructor
# and is always called when an object is created.

print(ciam)
