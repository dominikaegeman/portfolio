

class ThisDog:
    def __init__(self,  name):
        self.dog_name = name
        self.amount_of_meatballs_given = 0
        self.amount_of_meatballs_received = 0

    def transferring_meatballs(self, dog_with_gift):
        dog_with_gift.amount_of_meatballs_received += 1
        self.amount_of_meatballs_given += 1


dog_1 = ThisDog("Jen")
dog_2 = ThisDog("Zakierek")

dog_1.transferring_meatballs(dog_2)

print(f"{dog_1.dog_name} gave {dog_1.amount_of_meatballs_given} meatball")
print(f"{dog_1.dog_name} received {dog_1.amount_of_meatballs_received} meatball")
print(f"{dog_2.dog_name} gave {dog_2.amount_of_meatballs_given} meatball")
print(f"{dog_2.dog_name} received {dog_2.amount_of_meatballs_received} meatball")
