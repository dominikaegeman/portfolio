import turtle 
import pandas
# Turtle only works with a gif file

screen = turtle.Screen()
screen.title("U.S. States Game")
path = "./us-states-game-start/blank_states.gif"
screen.setup(700, 500)
screen.bgcolor("white")
screen.addshape(path)

turtle.shape(path)

data = pandas.read_csv("./us-states-game-start/all_states.csv")
allstates = data.state.to_list() # It will get us a Data Series so the first column   
guessed_states = []


while len(guessed_states) < 50:
    answer_state = screen.textinput(title=f" {len(guessed_states)}/50 States Correct", \
        prompt="What\'s another state\'s name? ").title() 
    # Title() method makes the first letter a capital letter so from ohio to Ohio
    print(answer_state)

    if answer_state == "Exit": # Remember here Exit must start with the capital letter because \
        # the previous input is lowercase letters with the capital letter on the beginning
        missing_states = []
        for state in allstates:
            if state not in guessed_states:
                missing_states.append(state)
        print(missing_states)
        new_data = pandas.DataFrame(missing_states)
        new_data.to_csv("states_to_learn.csv")
        
        break
        
    if answer_state in allstates:
        guessed_states.append(answer_state)
        t = turtle.Turtle()
        t.hideturtle()
        t.penup()
        state_data = data[data.state == answer_state] # Now we will receive the row in which the anwered states is found
        t.goto(int(state_data.x), int(state_data.y))
        # t.write(answer_state)
        t.write(state_data.state.item()) # It allows us to display the actual value so for example Ohio without the object \
            # and without further unncessary documents
            














turtle.mainloop() # It is an equivalent to screen.exitonclick() but mainloop() is more practical




