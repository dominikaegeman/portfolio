"""
Classes:
Snake
Food
Scoreboard
"""

from turtle import Screen, Turtle
# The turtle is grey out because we do not use this class here so you can delete it
from snake import Snake
from food import Food
from score_board import ScoreBoard
import time


screen = Screen()
screen.setup(width=600, height=600)
screen.colormode(255)
screen.bgcolor(166, 186, 232)
screen.title("My Snake Game")
screen.tracer(0)  # Turn off the tracer

snake = Snake()
food = Food()
score_board = ScoreBoard()

screen.listen()
screen.onkey(snake.snake_up, "Up")
screen.onkey(snake.snake_down, "Down")
screen.onkey(snake.snake_left, "Left")
screen.onkey(snake.snake_right, "Right")


game_is_on = True
while game_is_on:
    screen.update()
    time.sleep(0.1)
    snake.move()

# Detect collision with food
# distance method (turtle.distance)
    if snake.head.distance(food) < 35:
        print("Nom nom Nom")
        # the food is 10 x 10
        food.refresh()
        snake.extend()
        score_board.increase_score()

    # Detect collision with wall
    if snake.head.xcor() > 297 or snake.head.xcor() < -297 or snake.head.ycor() > 297 or snake.head.ycor() < -297:
        game_is_on = False
        score_board.game_over()

    # Detect collision with tail
    for segment in snake.segments[1:]:
        if snake.head.distance(segment) < 10:
            game_is_on = False
            score_board.game_over()
    # If head collides with any segment in tail:
    # trigger game_over

# 1,2,3 start=1, stop=3, step=+1
# 3,2,1 start=3, stop=1, step= -1
# BUT: It is from C language and keywords do not work in Python

screen.exitonclick()
