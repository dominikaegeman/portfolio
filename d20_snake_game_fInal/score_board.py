from turtle import Turtle
ALIGNMENT = "center"
FONT = ("Courier", 15, "bold")


class ScoreBoard(Turtle):

    def __init__(self):
        super().__init__()
        self.score = 0
        self.hideturtle()
        self.update_score()

    def update_score(self):
        self.color("white")
        self.penup()
        self.goto(0, 260)
        self.write(f"Score: {self.score}", True, align=ALIGNMENT, font=FONT)

    def increase_score(self):
        self.score += 1
        self.clear()
        self.update_score()

    def game_over(self):
        self.goto(0, 0)
        self.write("GAME OVER", align=ALIGNMENT, font=FONT)

# Font info is added as a tuple
