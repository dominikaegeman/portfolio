import tkinter



def label_settings(**kwargs):
    """ Function that takes arguments
        Warning: It will overwrite font, background, foreground. Warning important if function is used without if statements provided below
        
        
    Returns:

        dict: prepared arguments for label
        
    """
    if "font" not in kwargs.keys(): 
        kwargs["font"] = ("Consolas", 15, "bold")
    
    if "background" not in kwargs.keys():
        kwargs["background"] = "mint cream"
    
    if "foreground" not in kwargs.keys():
        kwargs["foreground"] = "dark slate gray"
    
    return kwargs



window = tkinter.Tk()
window.title("Mile to Km Converter")
window.minsize(width=100, height=80)
window.config(padx=20, pady=20, background="mint cream")



label_is_equal = tkinter.Label(cnf=label_settings(text="is equal to"))
label_is_equal.grid(column=0, row=1, pady=20)



label_miles = tkinter.Label(cnf=label_settings(text="Miles"))
label_miles.grid(column=2, row=0)

label_converted_measurement = tkinter.Label(cnf=label_settings(text=""))
label_converted_measurement.grid(column=1, row=1, pady=20)

# "%s".format("Km")


label_km = tkinter.Label(cnf=label_settings(text="%-5s" % "Km"))
label_km.grid(column=2, row=1, pady=20)

user_entry = tkinter.Entry(font=("Arial", 15), bg="azure2", width=5)
user_entry.grid(column=1, row=0, padx=20)

def calculatinng():
    number_in_miles = user_entry.get()
    result = float(number_in_miles) * 1.609344
    label_converted_measurement.config(text=result)
    

button_calculate = tkinter.Button(text="Calculate", command=calculatinng)
button_calculate.grid(column=1, row=2)









window.mainloop()