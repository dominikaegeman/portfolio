import pandas


df = pandas.read_csv("./d26_nato_alphabet/nato_phonetic_alphabet.csv")
df_dict = {row.letter: row.code for (index, row) in df.iterrows()}

user_word = input("Enter a word: ").upper()

result = [df_dict[letter] for letter in user_word]
print(result)


