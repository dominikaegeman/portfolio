'''
Why Environment Variables are used:
1. Convenience: You can modify variables wihout the need to touch the code 
2. Security: 

To make certain variables secret type in the terminal: 
export OWM_API_KEY=

Instead of api_key = ""
We have: 
OWM_API_KEY=
api_key = os.environ.get("OWM_API_KEY")

'''