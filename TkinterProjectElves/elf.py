import tkinter
import fpdf






class AddPdf(fpdf.FPDF):
    def __init__(self):
        super().__init__()
        

    def create_file(self, text):
            
        # Add a page
        self.add_page()
            
        # set style and size of font
        # that you want in the pdf
        self.set_font("Arial", size = 15)
            
        # create a cell
        self.cell(200, 10, txt = text,
            ln = 1, align = 'C')
            
        # add another cell
        # self.pdf.cell(200, 10, txt = "A Computer Science portal for geeks.",
        #     ln = 2, align = 'C')

        # save the pdf with name .pdf
        self.output("./TkinterProjectElves/Raport.pdf")  
        



nice_beige="#F2EEE4"
green = "#9AB166"


class Elf(tkinter.Canvas, tkinter.PhotoImage):
    def __init__(self):
        super().__init__()
        
        
        self.elf_dict = {
            "Willy" : [1000, 2000, 3000],
            "Allie": [4000],
            "Emilly": [5000, 6000],
            "Milly": [7000, 8000, 9000], 
            "Buddy": [10000]
            
        }
        
        self.chosen_elf_state = tkinter.IntVar()
        self.elf_chosen_by_user = ""

    

        self.elf_canvas = tkinter.Canvas(width= 1100,height=200,bg=nice_beige, highlightthickness=0)


        self.elf_1_photo = tkinter.PhotoImage(file="./TkinterProjectElves/elf_1.png")
        self.option_1 = tkinter.Radiobutton(image=self.elf_1_photo, value=1, \
            variable=self.chosen_elf_state, command=self.option_used)
        self.option_1.config(bg=nice_beige, activebackground="white", highlightthickness=0)
        self.option_1.place(x=10, y=500)
        

        self.elf_2_photo = tkinter.PhotoImage(file="./TkinterProjectElves/elf_2.png")
        self.option_2 = tkinter.Radiobutton(image=self.elf_2_photo, value=2, \
            variable=self.chosen_elf_state, command=self.option_used)
        self.option_2.config(bg=nice_beige, activebackground="white", highlightthickness=0)
        self.option_2.place(x=250, y=500)
        

        self.elf_3_photo = tkinter.PhotoImage(file="./TkinterProjectElves/elf_3.png")
        self.option_3 = tkinter.Radiobutton(image=self.elf_3_photo, value=3, \
            variable=self.chosen_elf_state, command=self.option_used)
        self.option_3.config(bg=nice_beige, activebackground="white", highlightthickness=0)
        self.option_3.place(x=500, y=480)
        
            

        self.elf_4_photo = tkinter.PhotoImage(file="./TkinterProjectElves/elf_4.png")
        self.option_4 = tkinter.Radiobutton(image=self.elf_4_photo, value=4, \
            variable=self.chosen_elf_state, command=self.option_used)
        self.option_4.config(bg=nice_beige, activebackground="white", highlightthickness=0)
        self.option_4.place(x=750, y=480)



        self.elf_canvas.grid(columnspan=5, rowspan=1, column=1, row=2)



    def calories_per_elf(self):
        print(f"The whole dictionary before adding calories together: {self.elf_dict}")
    
        for the_key in self.elf_dict:
            calories_per_elf = 0
            for the_value in self.elf_dict[the_key]:
                calories_per_elf += the_value
            self.elf_dict[the_key] = calories_per_elf
        
        print(f"Dictionary after adding calories together: {self.elf_dict}")
    
    
    def most_calories_finder(self):
        """This function return the elf who is carrying the most calories. 
        """
        self.biggest_calories_value = 0
        for elf in self.elf_dict:
            if self.elf_dict[elf] > self.biggest_calories_value:
                self.biggest_calories_value = self.elf_dict[elf]
        
        self.most_calories_elf = [key for key, value in self.elf_dict.items() if value == self.biggest_calories_value]
        self.most_calories_elf = ', '.join(self.most_calories_elf)


    def display_elf_most_calories(self, win):    
        if win == "yes":
            pdf = AddPdf()
            pdf.create_file(text="Congratulations! You are right. Milly is the elf who carries\nthe most calories! ")
    
            
        elif win != "yes":
            pdf = AddPdf()
            pdf.create_file(text="Ho, ho, ho! Please, try again :) ")



    def option_used(self):
        
        if self.chosen_elf_state.get() == 5:
            self.elf_chosen_by_user = "Buddy"
            
        elif self.chosen_elf_state.get() == 4:
            self.elf_chosen_by_user = "Milly"
        
        elif self.chosen_elf_state.get() == 3:
            self.elf_chosen_by_user = "Emilly"
        
        elif self.chosen_elf_state.get() == 2:
            self.elf_chosen_by_user = "Alice"
        
        elif self.chosen_elf_state.get() == 1:
            self.elf_chosen_by_user = "Willy"
        
        
        if self.elf_chosen_by_user == self.most_calories_elf:
            self.display_elf_most_calories(win="yes")
            
        
        elif self.elf_chosen_by_user != self.most_calories_elf:
            self.display_elf_most_calories(win="no")
    