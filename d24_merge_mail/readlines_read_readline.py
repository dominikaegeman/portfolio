"""
To read the last N Lines of a file in Python, the easiest way is to use the readlines() function and then access the last N elements of the returned list.

n = 5

with open("example.txt") as f:
    last_n_lines = f.readlines()[-n:]


https://theprogrammingexpert.com/python-difference-between-read-readline-readlines/
"""