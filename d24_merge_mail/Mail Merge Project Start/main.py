#TODO: Create a letter using starting_letter.txt 
#for each name in invited_names.txt
#Replace the [name] placeholder with the actual name.
#Save the letters in the folder "ReadyToSend".
    
#Hint1: This method will help you: https://www.w3schools.com/python/ref_file_readlines.asp
    #Hint2: This method will also help you: https://www.w3schools.com/python/ref_string_replace.asp
        #Hint3: THis method will help you: https://www.w3schools.com/python/ref_string_strip.asp

import time
import threading
import os

def removing_file(file_to_remove):

    ## If file exists, delete it ##
    if os.path.isfile(file_to_remove):
        os.remove(file_to_remove)
    else:    ## Show an error ##
        print("Error: %s file not found" %file_to_remove)

files = []
def creating_letters():
    lines = ""
    with open("./Input/Names/invited_names.txt", mode="r") as names:
        all_names = names.readlines()
        for name in all_names:
            name = name.strip()
            path_to_invitation = f"./Output/ReadyToSend/letter_for_{name}.txt"
            files.append(path_to_invitation)
            with open("./Input/Letters/starting_letter.txt", mode="r") as letter:
                message = letter.readlines()
                for line in message:
                    if line == message[0]:
                        message_replaced = line.replace("[name]", f"{name}")
                    elif line != message[0]:
                        lines += line
                with open(f"{path_to_invitation}", mode="w") as invitation_file:
                    invitation_file.write(f"{message_replaced}{lines}")
                    lines = ""
    return 1

def ask_user():
    remove = input("Would you like to remove files? ")
    if remove == "y":
        for path_file in files:
            removing_file(path_file)


def main():
    t1 = threading.Thread(target=creating_letters)
    t1.start()
    time.sleep(2)
    t2 = threading.Thread(target=ask_user)
    t2.start()



if __name__== "__main__":
    main()

