import tkinter as tk
import requests



def get_quote():
    response = requests.get(url="https://api.kanye.rest")
    response.raise_for_status()
    
    data = response.json()
    quote = data["quote"]
    background_canvas.itemconfig(quote_text, text=quote) # important itemconfig, specify which item/widget would you like to modify: here quote_text
    
    
    
window = tk.Tk()
window.title("Quotes")
window.geometry("600x600+800+200")
window.config(bg="#FFFBAC")

kanye = tk.PhotoImage(file="./d33_application_programming_interface_api/kanye.png")
kanye_label = tk.Label(image=kanye)

button_kanye = tk.Button(image=kanye, command=get_quote, background="#FFFBAC",activebackground="#F0E161", borderwidth=0, highlightthickness=0)
button_kanye.place(x=200, y=420)

background_canvas = tk.Canvas(background="#FFFBAC", width=400, height=400)
background_photo = tk.PhotoImage(file="./d33_application_programming_interface_api/background.png")
background_canvas.create_image(220, 200, image=background_photo)
quote_text = background_canvas.create_text(200, 100, text="Quote", font=("Arial", 15, "bold"), fill="white", width=200)
background_canvas.config(highlightthickness=0)
background_canvas.place(x=100, y=20)







window.mainloop()


'''
Another way to plan tkinter : 
# window = Tk()
# window.title("Kanye Says...")
# window.config(padx=50, pady=50)

# canvas = Canvas(width=300, height=414)
# background_img = PhotoImage(file="background.png")
# canvas.create_image(150, 207, image=background_img)
# quote_text = canvas.create_text(150, 207, text="Kanye Quote Goes HERE", width=250, font=("Arial", 30, "bold"), fill="white")
# canvas.grid(row=0, column=0)

# kanye_img = PhotoImage(file="kanye.png")
# kanye_button = Button(image=kanye_img, highlightthickness=0, command=get_quote)
# kanye_button.grid(row=1, column=0)



# window.mainloop()

'''