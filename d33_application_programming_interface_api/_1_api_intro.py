'''
API = Application Programming Interface 

It is a set of commands, functions, protocols and objects 
that programmers can use to create software or interact with an external system.


API Endpoint = Location the data is stored. Most of the time the URL. E.G.: http://api.open-notify.org/iss-now.json
API Request 

Response codes:
1XX = Hold On - sth is happening, it is not final
2XX = Everything has been successful, you should be getting the data you expected
3XX = You do not have the permission to access that 
4XX = 404 - the thing you are loooking for does not even exist
5XX = The server is down etc 

All possible codes you can see: https://www.webfx.com/web-development/glossary/http-status-codes/
Requests: HTTP for Humans™: https://docs.python-requests.org/en/latest/
Reverse Geocoding Convert Lat Long to Address: https://www.latlong.net/Show-Latitude-Longitude.html

'''


import requests

response = requests.get(url="http://api.open-notify.org/iss-now.json")
response.raise_for_status() #you will receive the number of an error

print(response.status_code)

data = response.json()
longitude = float(data["iss_position"]["longitude"])
latitude = float(data["iss_position"]["latitude"])

iss_ppositions = longitude, latitude
print(iss_ppositions)

