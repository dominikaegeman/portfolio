import pandas
import csv

from statistics import mean

data = pandas.read_csv("weather_data.csv")
data_list = data["temp"].to_list()

# First solution:
average = sum(data_list) / len(data_list)
print(average)

# Second solution:
average = mean(data_list)
print(average)

    