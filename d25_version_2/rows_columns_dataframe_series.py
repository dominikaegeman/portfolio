import pandas 

data = pandas.read_csv("weather_data.csv")
print(type(data))   # dataframe, a whole table is a dataframe
print(data["temp"]) # series, the column is a series

data_dict = data.to_dict()
print(data_dict)

temp_list = data["temp"].to_list()
# You can use data.temp too!
print(temp_list)

average_temp = sum(temp_list) / len(temp_list)
print(average_temp)

print(data["temp"].mean()) # This will print the average temperature. 

print(data["temp"].max()) # This is the max value in this data series.

print(data[data.day == "Monday"])

print(data[data.temp == data.temp.max()]) # The row with the highect tempearture 

monday = data[data.day == "Monday"] 
print(monday.condition)