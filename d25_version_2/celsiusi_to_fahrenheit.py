
import pandas
data = pandas.read_csv("weather_data.csv") 

# list_fahrenheit_temp = []
# for temprerature in data.temp:
#     fahrenheit = (temprerature * 9 / 5) + 32
#     list_fahrenheit_temp.append(fahrenheit)

# print(list_fahrenheit_temp)


column_in_fahrenheit = data.assign(temp_f = lambda x: x.temp * 9 / 5 + 32)
print(column_in_fahrenheit)

print("\nwith lambda below\n")

column_in_fahrenheit = data.assign(temp_f = data.temp * 9 / 5 + 32)
print(column_in_fahrenheit)


monday = data[data.day == "Monday"]
monday_temp = int(monday.temp)
monday_temp_F = monday_temp * 9/5 + 32
print(monday_temp_F )


# https://www.geeksforgeeks.org/applying-lambda-functions-to-pandas-dataframe/
