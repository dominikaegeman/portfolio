import pandas

age_sheet = pandas.read_csv("age_sheet.csv")
print(f"{age_sheet}\n")


# With lambda
age_added_new_column = age_sheet.assign(New_Age = lambda x: x.Age + 10)
print(f"\n{age_added_new_column}")


# Without lambda
age_added_new_column = age_sheet.assign(New_Age = age_sheet.Age + 10)
print(f"\n{age_added_new_column}")