import pandas

data = pandas.read_csv("weather_data.csv")
# print(data[data.day == "Monday"])

highest_temp = data.temp.max()

print(data[data.temp == highest_temp])
# print(data[data.temp == data.temp.max()]) 

# Condition for a particular day, for example Monday 

monday = data[data.day == "Monday"]
print(monday.condition)

