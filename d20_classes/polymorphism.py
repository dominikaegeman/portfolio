"""


POLYMORPHISM: It is an ability of object to behave in multiple form.
The most common use of polymorphism is Java, when a parent class reference type of variable is used to refer to a child class object.
In my framework E.g.: WebDriver driver = new ChromeDriver(); We use method overloading and overriding to achieve Polymorphism.
There are two types of achieving Polymorphism Upcasting & Downcasting Upcasting is casting a subtype to a supertype, going up in the inheritance tree.
It is done implicitly in order to use method available on any interface/class, the object should be of same class or of class implementing the interface.
WebDriver driver = new ChromeDriver(); or TakeScreenShot ts = new ChromeDriver();
Downcasting is casting to a subtype, going down in the inheritance tree.
It is done to access sub class features. It has to be done manually ChromeDriver driver = (ChromeDriver)Webdriver;

"""