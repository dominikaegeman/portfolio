import random
import turtle
import math
import os
import pygame.time

# Set up screen
wn = turtle.Screen()
wn.bgcolor("red")
wn.title("Simple Python Turtle Graphic Game (Class Version) ")
# wn.bgcolor()

# Registration of the shape
# wn.register_shape("heart.png")


class Game(turtle.Turtle):

    def __init__(self):
        turtle.Turtle.__init__(self)
        self.penup()
        self.hideturtle()
        self.speed(0)
        self.color("white")
        self.goto(-290, 310)
        self.score = 0

    def update_score(self):
        self.clear()
        self.write("Score: {}".format(self.score), False, align="left", font=("Arial", 14, "normal"))

    def change_score(self, points):
        self.score += points
        self.update_score()

    # def play_sound(self, filename):
    #     os.system("aplay {}&".format(filename))
    # & is used to allow the game continues, not to stop once the sounds is played
    # the filename has to be in the same folder as this directory


class Border(turtle.Turtle):
    """Border is a child of a Turtle class"""

    def __init__(self):
        turtle.Turtle.__init__(self)
        # Call the Turtle class constructor.
        # It will be the same self
        self.penup()
        self.hideturtle()
        self.speed(0)
        # self.speed(0) makes the borders visible right away
        self.color("white")
        self.pensize(5)

    def draw_border(self):
        self.penup()
        self.goto(-300, -300)
        self.pendown()
        self.goto(- 300, 300)
        self.goto(300, 300)
        self.goto(300, -300)
        self.goto(- 300, -300)


class Player(turtle.Turtle):
    """
    This class is a child/sub class of the Turtle() class
    Because of this we need initialise also a parent class (Turtle())
    """

    def __init__(self):
        turtle.Turtle.__init__(self)
        self.penup()
        self.speed(0)
        self.shape("triangle")
        self.color("white")
        self.speed = 1
# self.speed = 1 is not sth that is part of a Turtle class.
# This is sth we are adding, there is a speed method but it will be explained later

    def move(self):
        self.forward(self.speed)
        # Border Checking
        if self.xcor() > 290 or self.xcor() < -290:
            self.left(60)
        if self.ycor() > 290 or self.ycor() < -290:
            self.left(60)

    def turnleft(self):
        self.left(30)

    def turnright(self):
        self.right(30)

    def increasedspeed(self):
        self.speed += 1


class Goal(turtle.Turtle):
    # Polymorphism
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.penup()
        self.speed(0)
        self.color("green")
        self.shape("circle")
        self.speed = 1
        self.goto(random.randint(-250, 250), random.randint(-250, 250))
        self.setheading(random.randint(0, 360))

    def jump(self):
        self.goto(random.randint(-250, 250), random.randint(-250, 250))
        self.setheading(random.randint(0, 360))

    def move(self):
        self.forward(self.speed)
        # Border Checking
        if self.xcor() > 290 or self.xcor() < -290:
            self.left(60)
        if self.ycor() > 290 or self.ycor() < -290:
            self.left(60)

# Colission checking function
# Uses the Pythagorean Theorem to Measure The Distance Between Two Objects


def is_collision(t1, t2):
    a = t1.xcor() - t2.xcor()
    b = t1.ycor() - t2.ycor()
    distance = math.sqrt((a ** 2) + (b ** 2))

    if distance < 20:
        return True
    else:
        return False


# Create class instances
player = Player()
border = Border()
game = Game()

# Draw the border
border.draw_border()


# Create multiple goals
goals = []
for count in range(6):
    goals.append(Goal())

# Set keyboard bindings
turtle.listen()
turtle.onkey(player.turnleft, "Left")
turtle.onkey(player.turnright, "Right")
turtle.onkey(player.increasedspeed, "Up")

# Speed Up the Game
wn.tracer(2)
# wn.tracer(0) it stops the screen from being updated
# and then we use wn.update()to update only in a while loop


# Main Loop
while True:
    # wn.update()
    player.move()

    for goal in goals:
        goal.move()

    # Check for a collision between the player and goal
    if is_collision(player, goal):
        goal.jump()
        game.change_score(10)
        # game.play_sound("SOUND.mp3")
