import turtle
import random
color_list = [(202, 164, 110), (240, 245, 241), (236, 239, 243), (149, 75, 50), (222, 201, 136), (53, 93, 123), (170, 154, 41), (138, 31, 20), (134, 163, 184), (197, 92, 73), (47, 121, 86), (73, 43, 35), (145, 178, 149), (14, 98, 70), (232, 176, 165), (160, 142, 158), (54, 45, 50), (101, 75, 77), (183, 205, 171), (36, 60, 74), (19, 86, 89), (82, 148, 129), (147, 17, 19), (27, 68, 102), (12, 70, 64), (107, 127, 153), (176, 192, 208), (168, 99, 102)]


screen = turtle.Screen()
screen.setup(width=1000, height=800, startx=500, starty=50)
"""

width = szerokosc
height = wysokosc 
startx = prawo, lewo zaczynajac od prawego rogu jesli na plusie
startx = od lewego boku jesli np - 500
starty = gora dol zaczynajac od gory jesli na plusie
starty = od dolu jesli np -50

10 x 10

size of dots = 20

space between the dots - 50

100 dots in total
"""

dot = turtle.Turtle()
turtle.colormode(255)
dot.penup()
dot.hideturtle()
dot.setpos(-200, -300)
dot.pendown()
# dot.showturtle()

current_y = -300


def making_a_dot():
    colour = random.choice(color_list)
    dot.dot(20, colour)
    dot.penup()
    dot.forward(50)
    dot.pendown()


def starting_from_left():
    colour = random.choice(color_list)
    dot.penup()
    global current_y
    current_y += 50
    dot.setpos(-200, current_y)
    dot.pendown()
    # dot.dot(20, colour)
    print(dot.position()[0])


for element in range(101):
    if element == 0:
        starting_from_left()
    elif element == 100:
        making_a_dot()
    elif element == 101:
        making_a_dot()
    elif element % 10 == 0:
        making_a_dot()
        starting_from_left()
    else:
        making_a_dot()

screen.exitonclick()
