# This code will not work in repl.it as there is no access to the colorgram package here.###
# We talk about this in the video tutorials##


# Python interpreter | Then click on "+" to install the package


import colorgram

rgb_colours = []
colours = colorgram.extract("image.jpg", 30)
for colour in colours:
    r = colour.rgb.r
    g = colour.rgb.g
    b = colour.rgb.b
    new_colour = (r, g, b)
    rgb_colours.append(new_colour)

print(rgb_colours)


# rgb_colours.append(colour.rgb)

# The more closely the colours are to the 255, the more likely it is a shade of white
# so delete the ones connected to white (background)
