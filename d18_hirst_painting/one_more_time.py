import turtle
import random

turtle.colormode(255)
screen = turtle.Screen()
screen.setup(width=1000, height=900, startx=500, starty=80)
screen.bgcolor("light blue")

dot = turtle.Turtle()
color_list = [(202, 164, 110), (240, 245, 241), (236, 239, 243), (149, 75, 50), (222, 201, 136), (53, 93, 123), (170, 154, 41), (138, 31, 20), (134, 163, 184), (197, 92, 73), (47, 121, 86), (73, 43, 35), (145, 178, 149), (14, 98, 70), (232, 176, 165), (160, 142, 158), (54, 45, 50), (101, 75, 77), (183, 205, 171), (36, 60, 74), (19, 86, 89), (82, 148, 129), (147, 17, 19), (27, 68, 102), (12, 70, 64), (107, 127, 153), (176, 192, 208), (168, 99, 102)]
dot.penup()
dot.hideturtle()

dot.setpos(-400, -100)


def changing_direction():
    dot.setheading(90)
    dot.forward(50)
    dot.setheading(180)
    dot.forward(500)
    dot.setheading(0)


def making_dots():
    colour = random.choice(color_list)
    dot.dot(20, colour)
    dot.forward(50)


for element in range(1, 101):
    making_dots()
    if element % 10 == 0:
        changing_direction()
screen.exitonclick()
