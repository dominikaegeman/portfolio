# FileNotFound

try: 
    file = open("a_file.txt") # it can cause an error because we do not have this file
    a_dictionary = {"key":"value"}
    print(a_dictionary["non_existent_key"])
    
except:
    file = open("a_file.txt", "w") # If the file does not exist, the w mode will create it
    file.write("Soemthing")
    

# Inside the try block, it firsts is trying to open a_file.txt and if the file exists
# Then it will try to execute line number 6 and it fails so it will go to the except block, which is unrelated to the error that appered

# IT IS BETTER TO USE SPECIFIC EXCEPTION! 
try: 
    file = open("a_file.txt") # it can cause an error because we do not have this file
    a_dictionary = {"key":"value"}
    print(a_dictionary["non_existent_key"])
    
except FileNotFoundError:
    file = open("a_file.txt", "w") # If the file does not exist, the w mode will create it
    file.write("Soemthing")

except KeyError as error_message:
    print(f"That key {error_message} does not exist")

else: # It will be reached when try block was executed properly and no except has been needed
    content = file.read()
    print(content)

finally: # Code that will be run no matter what will happen
    file.close()
    print("File was closed ")