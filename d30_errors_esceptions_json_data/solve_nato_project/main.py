
# Keyword Method with iterrows()
# {new_key:new_value for (index, row) in df.iterrows()}

import pandas

data = pandas.read_csv("d30_errors_esceptions_json_data/solve_nato_project/nato_phonetic_alphabet.csv")
#TODO 1. Create a dictionary in this format:
phonetic_dict = {row.letter: row.code for (index, row) in data.iterrows()}
print(phonetic_dict)

#TODO 2. Create a list of the phonetic code words from a word that the user inputs.


correct_input = False
while correct_input == False:
    word = input("Enter a word: ").upper()
        
    try:
        output_list = [phonetic_dict[letter] for letter in word]


    except KeyError:
        print("Sorry, only letters in the alphabet are allowed.")
        
    else:
        correct_input = True
        print(output_list)
    
    
'''
Another way to solve this loop is throgh the usage of the break

while True:
    word = input("Enter a word: ").upper()
        
    try:
        output_list = [phonetic_dict[letter] for letter in word]


    except KeyError:
        print("Sorry, only letters in the alphabet are allowed.")
        
    else:
        print(output_list)
        break
        
        

'''

'''
It can also be solved by the usage of a function as below: 

def generate_phonetic():
    
    word = input("Enter a word: ").upper()
        
    try:
        output_list = [phonetic_dict[letter] for letter in word]


    except KeyError:
        print("Sorry, only letters in the alphabet are allowed.")
        generate_phonetic()
        
    else:
        print(output_list)
        
        
generate_phonetic()


'''