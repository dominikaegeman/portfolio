# FileNotFound

# KeyError
a_dicionary = {"key":"value"}
value = a_dicionary["non_existent_key"]

# IndexError
fruits_list = ["Apple", "Banana"]
fruit = fruits_list[3]

# TypeError
text = "abc"
print(text + 5) # a string + an int = TypeError

'''

try:        You executing sth that may cause an exception
except:     Do this if there was an exception / sth went wrong
else:       Do this if there were no exceptions / If you tried sth that may fail but it did not fail
finally:    Do this no matter what happens

'''

