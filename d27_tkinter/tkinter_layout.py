
"""
Tkinter Layout Managers:

The layout is necessary, without it the widnget will not be diplayed

A) pack (it packs each widget next to each other, start at the top and put the widget below the previous one)
pack(side="left") if you change all lwidgets, it will pack from the left to the right



B) place (is all aboout precise positioning)
my_label.place(x=0, y=0) it places at the top left corner


C) grid (you can divided it into columns and rows)
my_label.grid(column=0, row=0)



YOU CANNOT MIX PACK() AND GRID()

"""



import tkinter

window = tkinter.Tk()
window.title("Welcome!")
window.minsize(width=1000, height=1000)
window.config(padx=300, pady=300) # Here we are adding the padding  

my_label = tkinter.Label()
my_label.config(text="Text1", font=("Arial", 30))
my_label.grid(column=0, row=0)
# my_label.config(padx=100, pady=100) You can add a padding around the specific widget


first_button = tkinter.Button(text="Clicke Me: Button 1", width=20)
first_button.grid(column=2, row=0)


second_button = tkinter.Button(text="Clicke Me: Button 2", width=40)
second_button.grid(column=1, row=1)


user_entry = tkinter.Entry(width=40)
user_entry.insert(0,string="Start Here")
user_entry.grid(column=3, row=2)





































window.mainloop()








