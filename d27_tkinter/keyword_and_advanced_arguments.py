def myfunction(a:int, b:int, c:int):
    print(a)
    print(b)
    print(c)

# Here you need to specify the arguments 
myfunction(a=1, b=2, c=3)

# Arguments with default values
def myfunction(a=1, b=2, c=3):
    print(a)
    print(b)
    print(c)
    
# Here you do not need to specify the arguments
myfunction()

# But still you can change the value of b, and the rest will still take their default values
myfunction(b=5)

def foo(a, b=4, c=6): 
    print(a, b, c)

foo(4, 9)






