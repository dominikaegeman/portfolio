import tkinter

window = tkinter.Tk()
window.title("My GUI Program")
window.minsize(width=500, height=300)




# Label 

my_label = tkinter.Label(text="I am an Label ", font=("Arial", 24, "bold")) # bold, italis, ignore it (regular)
my_label.pack() # expand = True.False, side= "Left" etc

# my_label["text"] = "new text"  This one works too
my_label.config(text="New Text")    



# Button

def button_clicked():
    new_label = the_input.get()
    my_label.config(text=f"{new_label}")


button = tkinter.Button(text="Click Me", command=button_clicked)
button.pack()




# Entry

the_input = tkinter.Entry(width=10)
the_input.pack()









window.mainloop()
