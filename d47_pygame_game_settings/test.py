import pygame

# Initialize Pygame
pygame.init()

# Set the dimensions of the screen
screen_width = 800
screen_height = 600

# Create the screen
screen = pygame.display.set_mode((screen_width, screen_height))

# Set the color of the shape
color = (255, 0, 0)  # Red

# Set the starting position and size of the shape
x = 50
y = 50
width = 50
height = 50

# Set the speed at which the shape moves
speed = 4

x_dir = 0
y_dir = 0 


# Set the main loop of the game
running = True
clock = pygame.time.Clock()



while running:
    clock.tick(60)
    
    
    # Handle events
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    
    # Move the shape
    
    keys_pressed_simultaneously = pygame.key.get_pressed()
    
    if keys_pressed_simultaneously[pygame.K_LEFT]:
        x_dir = -1

    
        
    elif keys_pressed_simultaneously[pygame.K_RIGHT]:
        x_dir = 1

        
    else:
        x_dir = 0


    if keys_pressed_simultaneously[pygame.K_UP]:
        y_dir  = -1

        
    elif keys_pressed_simultaneously[pygame.K_DOWN]:
        y_dir = 1 

    
    else:
        y_dir=0

    
        
    
# TODO: If button pressed, we need to save this information 
    
    
    
# ciągle przesuwanie się, wyłączamy go gdy przycisk jest puszczony 
    
    # Check if the shape has gone off the screen
    if x > screen_width:
        x = 0

    if y > screen_height:
        y = 0
    
    x += speed * x_dir
    y += speed * y_dir
    


    # Fill the screen with black
    screen.fill((0, 0, 0))  # Black
    
    # Draw the shape on the screen
    pygame.draw.rect(screen, color, (x, y, width, height))
    
    # Update the screen
    pygame.display.update()
