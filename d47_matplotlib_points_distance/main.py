import math
import matplotlib.pyplot as plt

y_list = [20, 93, 72, 35, 54, 95, 25, 37, 29, 72, 65, 66, 49, 43, 35, 61, 97, 66, 64, 22, 83, 69, 19, 21, 69, 40, 35,
          81, 15, 41, 74, 12, 3, 65, 31, 12, 48, 68, 41, 40, 99, 13, 70, 30, 20, 35, 84, 96, 1, 93, 61, 83, 24, 27, 93,
          86, 96, 43, 10, 51, 27, 87, 40, 35, 83, 44, 15, 89, 71, 79, 25, 84, 43, 49, 66, 0, 88, 80, 4, 3, 74, 10, 41,
          45, 75, 34, 41, 44, 50, 99, 41, 37, 26, 6, 94, 94, 76, 48, 32, 42]
x_list = [number for number in range(1, 101)]


def plot_xy():
    x = [x + 1 for x in range(100)]
    y = y_list
    plt.scatter(x, y)
    plt.grid(True)
    plt.show()


class Point():
    def __init__(self, x, y, name):
        self.x = x
        self.y = y
        self.name = f"p{name}"

    def __str__(self):
        return f"{self.name} X:{self.x} Y:{self.y}"

    def distance(self, another_point):
        distance = math.sqrt((self.x - another_point.x) ** 2 + (self.y - another_point.y) ** 2)
        return distance

    def find_neighbours(self, max_distance, point_list):
        neighbours = []
        for point in point_list:
            # if point == self:
            #     continue We do not want to skip itself
            distance = self.distance(point)
            if distance <= max_distance:
                neighbours.append(point)
        return neighbours


# Example usage
if __name__ == '__main__':
    p1 = Point(1, 20, "1")
    point_list = [Point(x_list[i], y_list[i], i + 1) for i in range(len(x_list))]
    neighbours = p1.find_neighbours(20, point_list)
    print(f"Neighbors of {p1}\n {[str(neighbour) for neighbour in neighbours]}")
    plot_xy()

