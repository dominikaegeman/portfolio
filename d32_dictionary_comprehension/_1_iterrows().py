import pandas

data = pandas.read_csv("./d32_dictionary_comprehension/birthdays.csv")

'''
To iterate thourgh the rows of pandas DataFrame, it is good to use iterrows()

We create a dictionary that will consists of the keys (day and month). 
Then we add values to those keys from the csv file

'''
birthday_person = {}
for index, data_row in data.iterrows():
    birthday_person[(data_row.month, data_row.day)] = data_row

print(birthday_person)


'''
A dictionary comprehension can be used to achieve the same effect as above.
The structure is  new_dict = {new_key: new_value for (index, data_row) in data.iterrows()}

'''
birthdays_dict = {(data_row.month, data_row.day):data_row for(index, data_row) in data.iterrows()}


