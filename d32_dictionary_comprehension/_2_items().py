import pandas

df = pandas.DataFrame({'species': ['bear', 'bear', 'marsupial'],
                   'population': [1864, 22000, 80000]},
                  index=['panda', 'polar', 'koala'])
    
#print(df)


for label, content in df.items():
    print(f"label:{label}", sep=" ")
    print("###############")
    print(f"content:{content}", sep=" ")

'''
label : object
    The column names for the DataFrame being iterated over.
content : Series
    The column entries belonging to each label, as a Series.
    

'''