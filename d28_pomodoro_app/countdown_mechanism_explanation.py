"""
import time
count = 5
while True:
    time.sleep(1) # sleep for a second
    count+=1
    
But we are dealing with GUI so the event is needed! 

"""

import tkinter

window = tkinter.Tk()
window.title("Hey")

def say_sth(a,b,c,):
    print(a,b,c)
    
window.after(1000, say_sth, "Hello", 3,5)
# 1 second is 1000 ms, and here we are passing the ms and not seconds


window.mainloop()
