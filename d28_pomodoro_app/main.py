import tkinter
import math
# ---------------------------- CONSTANTS ------------------------------- #
PINK = "#e2979c"
RED = "#e7305b"
GREEN = "#9bdeac"
YELLOW = "#f7f5dd"
FONT_NAME = "Courier"
WORK_MIN = 25
SHORT_BREAK_MIN = 5
LONG_BREAK_MIN = 20
reps = 0
timer = None


# ---------------------------- TIMER RESET ------------------------------- # 

def reset_timer():
    window.after_cancel(timer)
    canvas.itemconfig(timer_text, text="00:00") # This is how the canvas configuration is changed
    timer_label.config(text="Timer")
    check_marks.config(text="")
    global reps
    reps = 0




# ---------------------------- TIMER MECHANISM ------------------------------- # 

def start_timer():
    global reps
    reps += 1
    work_sec = WORK_MIN * 60
    short_break_sec =  SHORT_BREAK_MIN * 60
    long_break_sec = LONG_BREAK_MIN
    
    if reps % 8 == 0:
        timer_label.config(text="Brake", fg=RED)
        count_down(long_break_sec)
        
    elif reps % 2 == 0:
        timer_label.config(text="Brake", fg=PINK)
        count_down(short_break_sec)
    
    else:
        timer_label.config(text="Work", fg=GREEN)
        count_down(work_sec)
        
    
    
    #count_down(5 * 60) # So instead of 5 seconds we will have 5 minutes


# ---------------------------- COUNTDOWN MECHANISM ------------------------------- # 
def count_down(count):
    
    """ 245 / 60 = 4 minutes
    245 % 60 = number of seconds remaining
    """
    count_min = math.floor(count / 60 ) # So if count_min is 3,6 the count_min will be 3 
    count_sec = count % 60
    if count_sec < 10 :
        count_sec = f"0{count_sec}"
    
    canvas.itemconfig(timer_text, text=f"{count_min}: {count_sec}")
    if count > 0:
        global timer
        
        timer = window.after(1000, count_down, count -1)
        # http://tcl.tk/man/tcl8.6/TclCmd/after.html
        
    else:
        start_timer()
        marks = ""
        worked_sessions = math.floor(reps / 2 ) # The number of worked session
        
        for _ in range(worked_sessions):
            marks += "✔"
        
        check_marks.config(text=marks)
            
            
            
            
            
# To change a Label Label.config(text=) but to change the Canvas we need to 

# ---------------------------- UI SETUP ------------------------------- #


window = tkinter.Tk()
window.title("🍅 Pomodoro 🍅")
# window.minsize(width=300, height=300)
window.config(padx=100, pady=50, bg=YELLOW)
# If there is no constant then use bg= "# "



start_button = tkinter.Button(text="Start", bg=RED, fg="#FFFFFF", activebackground=PINK, activeforeground="#FFFFFF", highlightthickness=0, command=start_timer)
start_button.grid(column=0, row=2)

timer_label = tkinter.Label(text="Timer", fg=GREEN, font=(FONT_NAME, 35, "bold"), bg=YELLOW)
timer_label.grid(column=1, row=0)

canvas = tkinter.Canvas(width=200, height=224, bg=YELLOW,highlightthickness=0)
tomato_img = tkinter.PhotoImage(file="./d28_pomodoro_app/tomato.png")
canvas.create_image(100, 112, image=tomato_img)
timer_text = canvas.create_text(100, 130, text="00:00", fill="white", font=(FONT_NAME, 35, "bold"))
canvas.grid(column=1, row=1)


revert_button = tkinter.Button(text="Reset", bg=RED, fg= "#FFFFFF", activebackground=PINK, activeforeground="#FFFFFF", highlightthickness=0, command=reset_timer)
revert_button.grid(column=2, row=2)

check_marks = tkinter.Label(fg=GREEN, bg=YELLOW, font=(FONT_NAME, 30))
check_marks.grid(column=1, row=3)

# highlightthickness=0 allows us to omit the borsers so the image or the button looks better!
