    
class A:
    def __init__(self):
        pass
    
    def aww(self):
        print("ciam")


class B:
    def __init__(self):
        pass

    def option_1(self):
        print("bubu")
    
    
class C:
    # Different way: 
    
    # settings = A()
    # options = B()
    
    def __init__(self):
        self.settings = A()
        self.options = B()
        
    
    def option_2(self):
        
        self.settings.aww()
        self.options.option_1()
        

if __name__ == "__main__":
    obj_c = C()
    obj_c.option_2()