import tkinter as tk
import PyPDF2
from PIL import ImageTk, Image
from tkinter.filedialog import askopenfile
from elf import Elf, AddPdf
from playsound import playsound



# def play():
#     playsound("./TkinterProjectElves/intro_sound.mp3")


nice_beige="#F2EEE4"
green = "#9AB166"       


    

def open_file():
    browse_text.set("Loading...")
    file=askopenfile(parent=root, mode="rb", title="Choose a file", filetypes=[("Pdf file","*.pdf")]) #rb - read only
    if file:
        read_pdf = PyPDF2.PdfFileReader(file)
        page = read_pdf.getPage(0)
        page_content = page.extractText()
        

        
        text_box = tk.Text(root, height=3, width=55, padx=15, pady=15, highlightthickness=4, bg="#F4EAD5", font=("Raleway", 15, ("italic", "bold")))
        text_box.config(highlightbackground="white")
        text_box.insert(1.0, page_content)
        text_box.tag_config("center", justify="center")
        text_box.tag_add("center", 1.0, "end")
        text_box.grid(columnspan=6,rowspan=4, column=1, row=0)
        browse_text.set("Submit")
        #play()
        





if __name__ == "__main__":
    root = tk.Tk()
    root.title("Merry Christmas")
    # root.geometry('%dx%d+%d+%d' % (w, h, x, y))
    # root.geometry('%dx%d+%d+%d' % (600, 600, 600, 250))
    root.geometry('+%d+%d' %(325, 165))
    
    # header area  - Logo & browse button 
    header = tk.Canvas(root, width=1100, height=350, bg="#9AB166")
    header_photo = tk.PhotoImage(file="./TkinterProjectElves/logo.png")
    header.create_image(550, 150, image=header_photo)
    header.grid(columnspan=5, rowspan=2, row=0)

    # main content - text and image extraction
    main_content = tk.Frame(root, width=1100, height=400, bg=nice_beige)
    main_content.grid(columnspan=5, rowspan=2, row=2)

    elf = Elf()
    elf.calories_per_elf()
    elf.most_calories_finder()



    browse_text =tk.StringVar()
    browse_button = tk.Button(root, textvariable=browse_text, font=("Raleway", 15, ("italic", "bold")), \
        bg=green, fg="black", height=2,width=15, command=lambda:open_file(), highlightthickness=0, \
            activebackground="black", activeforeground=nice_beige)
    browse_text.set("Submit")
    browse_button.grid(columnspan=5,rowspan=4,column=1, row=3)




# instructions
# instructions = tk.Label(root, text="Select a PDF file on your computer to extract all its text", \
#     font=("Raleway", 15, "normal"), bg=green)
# instructions.grid(columnspan=3, column=2, row=1) # Span across al 3 of my columns



    root.mainloop()