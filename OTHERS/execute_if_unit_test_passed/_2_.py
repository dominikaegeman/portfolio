import unittest
from main import find_sum

class TestSum(unittest.TestCase):
    def test_sum(self):
        self.assertEqual(find_sum([1,2,2]), 6, "Should be 6 ")
        self.assertEqual(find_sum([1,2,3,4]), 10, "Should be 10")


if __name__ == '__main__':
    unittest.main()
