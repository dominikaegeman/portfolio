import unittest

def find_sum(numbers: list):
    return sum(numbers)


if __name__ == '__main__':
    tests = unittest.TestLoader().discover("test")
    
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    
    if result.wasSuccessful():
        print(find_sum([1,2,3,4,5]))
        print("great")
    else:
        print("Tests failed")   