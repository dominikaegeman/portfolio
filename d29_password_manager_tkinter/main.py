import tkinter
import os
from tkinter import messagebox
# tkinter * import all classes and contans but not import the module
import random
import pyperclip


'''

>>> import pyperclip
>>> pyperclip.copy('The text to be copied to the clipboard.')
>>> pyperclip.paste()
'The text to be copied to the clipboard.'


'''
# ---------------------------- PASSWORD GENERATOR ------------------------------- #
# Password Generator Project

def generate_password():
    letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
            'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']

    nr_letters = random.randint(8, 10)
    nr_symbols = random.randint(2, 4)
    nr_numbers = random.randint(2, 4)

    #List comprehension
    password_letters = [random.choice(letters) for _ in range(nr_letters)]
    password_symbols = [random.choice(symbols) for _ in range(nr_symbols)]
    password_numbers = [random.choice(numbers) for _ in range(nr_numbers)]

    password_list = password_letters + password_symbols + password_numbers
    random.shuffle(password_list)


# password_list = []

# password_list = [random.choice(letters) for char in range(nr_letters)]
# # for char in range(nr_letters):
# #     password_list.append(random.choice(letters))

# password_list += [random.choice(symbols) for char in range(nr_symbols)]
# # for char in range(nr_symbols):
# #     password_list += random.choice(symbols)

# password_list += [random.choice(numbers) for char in range(nr_numbers)]
# # for char in range(nr_numbers):
# #     password_list += random.choice(numbers)




# password = ""
# for char in password_list:
#     password += char

# print(f"Your password is: {password}")

    password ="".join(password_list)
    password_entry.insert(0, password)  
    pyperclip.copy((password))
    
    # Another way to copy the password to the clipboard:   windows.clipboard_append(password_entry.get())


# ---------------------------- SAVE PASSWORD ------------------------------- #

def save():
    email_inserted = email_entry.get()
    password_inserted = password_entry.get()
    website_inserted = website_entry.get()

    path = f"{os.getcwd()}/d29_password_manager_tkinter/data.txt"

    # messagebox.showinfo(title="Title", message="Message")
    # messagebox.askyesno()
    # messagebox.askretrycancel()
    # messagebox.showerror()
    # messagebox.showwarning()
    # messagebox.askquestion()
    # messagebox.askyesnocancel()

    if len(email_inserted) <= 0 or len(password_inserted) <= 0 or len(website_inserted) <= 0:
        messagebox.showinfo(
            title="Ooops", message="Plase, do not leave any of the fields empty!")

    else:
        is_okay = messagebox.askokcancel(
            title=website_inserted, message=f"These are the details entered: \nEmail: {email_inserted} \n Password: {password_inserted} \nIs it okay to save?")

        if is_okay:
            with open(path, "a+") as data_file:
                data_file.write(
                    f"{website_inserted} | {email_inserted} | {password_inserted}\n")

    '''
    The a creates a new file or opens an existing file for writing ; \
        the file pointer position at the end of the file . \
            The a+ creates a new file or opens an existing file for reading and writing, \
                and the file pointer position at the end of the file 
    '''
    email_entry.delete(0, "end")
    email_entry.insert(0, "the_most_common_email@gmail.com")
    password_entry.delete(0, "end")
    # If end omitted, then only the first character will be removed
    website_entry.delete(0, "end")
    # website_entry.delete(0, END)


# ---------------------------- UI SETUP ------------------------------- #
bg_colour = "#2C3333"
logo_img_path = f"{os.getcwd()}/d29_password_manager_tkinter/logo.png"
if os.name == "nt":
    logo_img_path = logo_img_path.replace("/", "\\")

windows = tkinter.Tk()
windows.title("Password Generator")
# windows.config(padx=20, pady=20, bg="#2C3333", height=1000, width=1000)
windows.config(padx=20, pady=20, bg=bg_colour)

canvas = tkinter.Canvas(width=200, height=200,
                        bg=bg_colour, highlightthickness=0)
logo_img = tkinter.PhotoImage(file=logo_img_path)
canvas.create_image(100, 100, image=logo_img)
# The x position of the center of the image will be 100, y position is also going to be 100
canvas.grid(row=0, column=1)

website_label = tkinter.Label(
    text="Website:", bg=bg_colour, fg="#ffffff", highlightthickness=0)
website_label.grid(row=1, column=0)

email_label = tkinter.Label(text="Email/Username:",
                            bg=bg_colour, fg="#ffffff", highlightthickness=0)
email_label.grid(row=2, column=0)

password_label = tkinter.Label(
    text="Password:", bg=bg_colour, fg="#ffffff", highlightthickness=0)
password_label.grid(row=3, column=0)

website_entry = tkinter.Entry(width=35, highlightthickness=0)
website_entry.focus()

website_entry.grid(row=1, column=1, columnspan=2)

email_entry = tkinter.Entry(width=35, highlightthickness=0)
email_entry.insert(0, "the_most_common_email@gmail.com")
'''
email_entry.insert(END)
END is the very last character- this allows the user to provide the input after the already
stated text

'''

email_entry.grid(row=2, column=1, columnspan=2)

password_entry = tkinter.Entry(width=15)
password_entry.grid(row=3, column=1)

generate_password_button = tkinter.Button(text="Generate Password", bg="white", command=generate_password)
generate_password_button.grid(row=3, column=2)

add_button = tkinter.Button(text="Add", width=36, command=save)
add_button.grid(row=4, column=1, columnspan=2)
windows.mainloop()
