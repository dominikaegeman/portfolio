from turtle import Turtle


class Border(Turtle):
    def __init__(self):
        super().__init__()
        self.hideturtle()
        self.shape("square")
        self.shapesize(stretch_wid=2, stretch_len=2)
        self.penup()
        self.setposition(410, 310)


class Ball(Turtle):
    def __init__(self, heading):
        super().__init__()
        self.shape("circle")
        self.color("white")
        self.setheading(heading)
        self.edge = "no"

    def move_to_right_edge(self):
        # if self.xcor() == 383.34504482270034 and self.ycor() == 288.8712111129832:
        #     self.edge = "yes"

        self.penup()
        self.forward(10)

    def game_over(self):
        self.penup()
        self.setposition(0, 0)
        self.color("grey")
        self.write("The game is over", True, align="center", font=("Courier", 30, "normal"))
