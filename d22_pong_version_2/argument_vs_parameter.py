from turtle import Turtle, Screen
import os

PATH = "~/Desktop/100 DAYS OF PYTHON/parameter_vs_argument.gif"
screen = Screen()
screen.bgcolor("black")
screen.setup(1000, 1000)
screen.addshape(os.path.expandvars(PATH))

image = Turtle()
image.shape(os.path.expandvars(PATH))

screen.exitonclick()
