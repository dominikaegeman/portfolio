from turtle import Screen, Turtle
import time
from paddle import Paddle
from ball import Ball, Border

screen = Screen()
screen.bgcolor("orange")
screen.setup(800, 600)
screen.title("Pong Game")
screen.listen()
screen.tracer(0)


r_paddle = Paddle((350, 0))
l_paddle = Paddle((-350, 0))
# Here we have got a tuple as an argument to have just a one parameter in a class
ball = Ball(37)
boarder = Border()

game_on = True
while game_on:
    time.sleep(0.1)
    screen.update()

    if ball.distance(boarder) < 10:
        ball.edge = "yes"

    if ball.edge != "yes":
        ball.move_to_right_edge()

    elif ball.edge == "yes":
        screen.reset()
        ball.game_over()
        game_on = False
    #  screen.tracer(0) plus screen.update in a while allows not to show the process of turtle's relocation to the position set
    screen.onkey(fun=r_paddle.up, key="Up")
    screen.onkey(fun=r_paddle.down, key="Down")
    screen.onkey(fun=l_paddle.up, key="w")
    screen.onkey(fun=l_paddle.down, key="s")

screen.exitonclick()
