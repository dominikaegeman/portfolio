from turtle import Turtle


class Ball(Turtle):
    def __init__(self, heading):
        super().__init__()
        self.shape("circle")
        self.color("white")
        self.setheading(heading)
        self.edge = "no"
        # self.penup()

    def move(self, x_, y_):
        new_x = self.xcor() + x_
        new_y = self.ycor() + y_
        self.goto(new_x, new_y)

    def game_over(self):
        self.penup()
        self.setposition(0, 0)
        self.color("grey")
        self.write("The game is over", True, align="center", font=("Courier", 30, "normal"))
