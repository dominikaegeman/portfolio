from turtle import Screen, Turtle
import time
from paddle import Paddle
from ball import Ball

screen = Screen()
screen.bgcolor("orange")
screen.setup(800, 600)
screen.title("Pong Game")
screen.listen()
screen.tracer(0)


r_paddle = Paddle((350, 0))
l_paddle = Paddle((-350, 0))  # Here we have got a tuple as an argument to have just a one parameter in a class
ball = Ball(37)

#  screen.tracer(0) plus screen.update in a while allows not to show the process of turtle's relocation to the position set

screen.onkey(fun=r_paddle.up, key="Up")
screen.onkey(fun=r_paddle.down, key="Down")
screen.onkey(fun=l_paddle.up, key="w")
screen.onkey(fun=l_paddle.down, key="s")

cur_dir_x = 5
cur_dir_y = 5

game_on = True
while game_on:
    screen.update()
    time.sleep(0.1)
    ball.move(cur_dir_x, cur_dir_y)

    # Detect a collision with a wall
    if ball.ycor() > 300 or ball.ycor() < -300:
        pass
    # However, change it because -5 and 5 is required depending on the y.cor()

    elif ball.ycor() < r_paddle.ycor() + 51 and ball.ycor() > r_paddle.ycor() - 51 and ball.xcor() > r_paddle.xcor() - 11:
        print(r_paddle.position())
        cur_dir_x = -5

    elif ball.ycor() < l_paddle.ycor() + 51 and ball.ycor() > l_paddle.ycor() - 51:
        if ball.xcor() > l_paddle.xcor() - 11:
            cur_dir_x = 5

    if ball.xcor() == 390 or ball.xcor() == -390:
        screen.reset()
        ball.game_over()
        game_on = False
screen.exitonclick()
