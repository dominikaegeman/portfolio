from turtle import Turtle


class Paddle(Turtle):

    def __init__(self, position):
        super().__init__()
        self.shape("square")
        self.hideturtle()
        self.color("white")
        self.shapesize(stretch_wid=5, stretch_len=1)
        # All turtle starts as 20 by 20
        # To receive 100 x 20; we need to stretch the width by 5 and receive the len as it is (by one)
        self.penup()
        self.setposition(position)
        self.showturtle()

    def up(self):
        new_y_position = self.ycor() + 50
        self.goto(self.xcor(), new_y_position)

    def down(self):
        new_y_position = self.ycor() - 50
        self.goto(self.xcor(), new_y_position)
