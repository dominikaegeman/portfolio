from turtle import Screen, Turtle
import time
from paddle import Paddle
from ball import Ball
from scoreboard import Scoreboard

screen = Screen()
screen.bgcolor("orange")
screen.setup(800, 600)
screen.title("Pong Game")
screen.listen()
screen.tracer(0)


r_paddle = Paddle((380, 0))
l_paddle = Paddle((-380, 0))  # Here we have got a tuple as an argument to have just a one parameter in a class
ball = Ball(37)
scoreboard = Scoreboard()


#  screen.tracer(0) plus screen.update in a while allows not to show the process of turtle's relocation to the position set

screen.onkey(fun=r_paddle.up, key="Up")
screen.onkey(fun=r_paddle.down, key="Down")
screen.onkey(fun=l_paddle.up, key="w")
screen.onkey(fun=l_paddle.down, key="s")


game_on = True
while game_on:
    screen.update()
    time.sleep(ball.move_speed)
    ball.move()

    # Detect a collision with a wall. The ball is 20 weight so 300 - 20 = 280
    if ball.ycor() > 280 or ball.ycor() < -280:
        ball.bounce_y()
    # However, change it because -5 and 5 is required depending on the y.cor()

    # Detect collision with paddles:
    # 380(x position of paddle) - 20
    if ball.distance(r_paddle) < 50 and ball.xcor() > 360 or ball.distance(l_paddle) < 50 and ball.xcor() < -360:
        ball.bounce_x()
        ball.move_speed *= 0.9

    # Detect when r_paddle misses:
    if ball.xcor() > 385:
        ball.reset_position()
        scoreboard.l_point()

    # Detect when l_paddle misses:
    if ball.xcor() < -385:
        ball.reset_position()
        scoreboard.r_point()


# If the screen is 800 then it is 400 up
# So if the paddle is on 350, it is actually between 340 to 360 (because the len is 20)
# So if the ball reaches the 380, the paddle is definitely not touched because it it was, then it would bounce
# distance method is measuring the distance between the centers of two turtles


screen.exitonclick()
