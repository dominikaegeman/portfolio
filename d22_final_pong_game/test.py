import time
import turtle

turtle.tracer(False)  # stop animation and don't update content on screen

wn = turtle.Screen()
sasha = turtle.Turtle()

length = 12
for i in range(65):
    turtle.update()
    time.sleep(0.1)  # To update the screen while drawing
    sasha.forward(length)
    sasha.right(120)
    length = length + 10


turtle.done()


"""

import turtle

turtle.tracer(False) # stop animation and don't update content on screen

wn = turtle.Screen()
sasha = turtle.Turtle()

length = 12
for i in range(65):
    sasha.forward(length)
    sasha.right(120)
    length = length + 10

turtle.update() # update content on screen once drew

turtle.done()

"""
