
# TODO: Find an asbolute path:
# with open("/home/dominika/Desktop/file.txt") as file:
#     message = file.read()
#     print(message)

""""
One of the peculiarities about file paths in Windows and in a Mac, 

In a Mac: The path each of the folder is separated by the forward slash

In a Windows: The path each of the folder is separated by the backslash

In Python: you can use forward slash

"""

# TODO: Find a relative path:
with open("../../../Desktop/file.txt") as file:
    message = file.read()
    print(message)


"""
The main difference between an absolute file path and relative file path is the absolute file path is always 
relative to the root of the computer.

The relative file path is relative to your current working directory (it depends where you are and where you are trying 
to get to) 

"""


"""
QUIZ:
1) If you are writing code inside the main.py file to open the quiz.txt what would be the relative file path?
    - Although you can also write this as "./quiz.txt", the "./" is optional.



"""