file = open("my_file.txt")
contents = file.read()
print(contents)
file.close()  # It is better to close down the file to avoid causing a burden to a computer.

# However, we no longer have to remember to close down the file if we use the option below:
with open("my_file.txt") as file:
    contents = file.read()
    print(contents)

# with = as soon as it notices that we are done with it, it will close down the file


# with open ("my_file_2.txt") as file2:
#     file2.write("New text")
# This opens the file in a read-only mode

with open("my_file_2.txt", mode="w") as file2:
    file2.write("New text")

with open("my_file_2.txt", mode="a") as file2:  # We append the text and thereofre, the deletion does not happen
    file2.write("\nNew text2")


# If you are trying to open the folder with mode = "w" and that file does not exist,
# it will be created for you from scratch
