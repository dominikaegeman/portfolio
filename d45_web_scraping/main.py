import requests
from bs4 import BeautifulSoup

URL = "https://web.archive.org/web/20200518073855/https://www.empireonline.com/movies/features/best-movies-2/"

# Write your code below this line 👇


response = requests.get(URL)
movies_website = response.text

soup = BeautifulSoup(movies_website, "html.parser")
#print(soup.prettify())

movies = soup.find_all(name="h3", class_="title")

# for movie in movies[::-1]:
#     the_movie_title = movie.getText()
#     # the_movie_title = the_movie_title.split(" ")
#     # the_movie_title = " ".join(the_movie_title[1:])
#     print(the_movie_title)

movie_titles = [movie.getText() for movie in movies]
all_movies = movie_titles[::-1]


# for n in range(len(movie_titles)-1, -1, -1):
#     print(movie_titles[n])

with open("./d45_web_scraping/MoviesToWatch/movie.txt", mode="w") as file:
    for movie in all_movies:
        file.write(f"{movie}\n")