import turtle


class SnakeBody(turtle.Turtle):

    def __init__(self, num):
        turtle.Turtle.__init__(self)
        if num == 0:
            self.construct_head()
        else:
            self.construct_body()
        self.__current_x = 0
        self.__current_y = 0

    def construct_head(self):
        self.hideturtle()
        self.penup()
        self.setposition(0, 0)
        self.shape("arrow")
        self.color("white")
        self.shapesize(1, 1)
        self.showturtle()
        return self

    def construct_body(self):
        self.hideturtle()
        self.penup()
        self.setposition(0, 0)
        self.shape("square")
        self.color("white")
        self.shapesize(1, 1)
        self.showturtle()
        return self

    def set_position_x(self, new_x_position):
        self.__current_x = new_x_position
        self.setposition(new_x_position, self.__current_y)

    def set_position_y(self, new_y_position):
        self.__current_y = new_y_position
        self.setposition(self.__current_x, new_y_position)


class Movements(turtle.Turtle):

    def __init__(self, myscreen):
        turtle.Turtle.__init__(self)

        self.screen = myscreen

    def moving_forward(self):
        self.setheading(90)

    def movement_options(self):
        self.screen.listen()
        self.screen.onkey(key="Up", fun=self.moving_forward)


class StartGameSnake:
    __body_types_part = []
    # Everything added to the single object is written here. COnsequently, we will receive the list of all objects

    @staticmethod
    def __initwindow():
        # Private method
        screen = turtle.Screen()
        screen.setup(600, 600)
        screen.colormode(255)
        screen.bgcolor(166, 186, 232)
        screen.title("My Snake Game")

        return screen

    def render(self):
        pass

    def update(self):
        pass

    def run(self):
        # TODO: Exit on escape
        # TODO: Make three methods: update and render (check it), user_input
        while True:
            self.render()
            self.update()

    def __init__(self):
        self.display_screen = self.__initwindow()

        self.movements = Movements(self.display_screen)
        self.__body_types_part.append(SnakeBody(0))
        self.__body_types_part.append(SnakeBody(1))
        self.__body_types_part.append(SnakeBody(1))

        length_of_snake = len(self.__body_types_part)

        for index, object in enumerate(self.__body_types_part):
            index = length_of_snake - index - 1
            object.set_position_x(new_x_position=index * 20)


if __name__ == '__main__':
    snake_game = StartGameSnake()
    snake_game.run()




