import turtle


class BodyOfSnake:
    def __init__(self):
        self.screen = turtle.Screen()
        self.screen.setup(600, 600)
        self.screen.colormode(255)
        self.screen.bgcolor(166, 186, 232)
        self.screen.title("My Snake Game")
        self.screen.exitonclick()

    def construct_head(self):
        self.head = turtle.Turtle()
        self.head.shape("arrow")
        self.head.shapesize(1)
        self.head.color("white")
        self.head.setposition(0, 0)
        print(self.head.xcor())

    def position_of_head(self):
        x_position = self.head.xcor()
        y_position = self.head.ycor()
        print(x_position, y_position)

    def construct_tail(self):
        self.tail = turtle.Turtle()
        self.tail.shape("square")
        self.tail.shapesize(10)
        self.tail.color("orange")
        x_position = self.head.xcor() + 10
        y_position = self.head.ycor()
        self.tail.setposition(x_position, y_position)

    def position_of_tail(self):
        x_position = self.tail.xcor()
        y_position = self.head.xcor()
        print(x_position, y_position)


class StartGame:
    def __init__(self):
        self.object_constructing = BodyOfSnake()
        self.object_constructing.construct_head()
        self.object_constructing.position_of_head()
        self.object_constructing.construct_tail()
        self.object_constructing.position_of_tail()


if __name__ == '__main__':
    snake_game = StartGame()
