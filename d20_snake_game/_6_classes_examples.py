class ClassA(object):
    def __init__(self):
        self.var1 = 1
        self.var2 = 2

    def method_a(self):
        self.var1 = self.var1 + self.var2
        return self.var1


class ClassB(ClassA):
    def __init__(self, class_a):
        super().__init__()
        self.var1 = class_a.var1
        self.var2 = class_a.var2


object1 = ClassA()
_sum = object1.method_a()
object2 = ClassB(object1)
print(_sum)
print(object2.var2)
