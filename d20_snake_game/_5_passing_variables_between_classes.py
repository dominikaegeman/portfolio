

# To pass the variables between the classes you can use two approaches.


# THE FIRST APPROACH:

"""
var1 and var2 are instance variables.
That means that you have to send the instance of ClassA to ClassB in order for ClassB to access it, i.e:

"""


class ClassA(object):
    def __init__(self):
        self.var1 = 1
        self.var2 = 2

    def method_a(self):
        self.var1 = self.var1 + self.var2
        return self.var1


class ClassB(ClassA):
    def __init__(self, class_a):
        ClassA.__init__(self)
        self.variable1 = class_a.var1
        self.variable2 = class_a.var2


object1 = ClassA()
# object1.methodA()
# new_var1 = object1.methodA()
object2 = ClassB(object1)

# print(new_var1)
print("This is the first approach")
print(object2.variable1, object2.variable2)






# THE SECOND APPROACH:
# You can access var1 and var2 without sending object1 as a parameter to ClassB.
# On the other hand - if you were to use class variables, you could access var1 and var2 without sending object1 as a parameter to ClassB.
# Note, however, that class variables are shared among all instances of its class.

class ClassC(object):
    var1 = 0
    var2 = 0

    def __init__(self):
        ClassC.var1 = 3
        ClassC.var2 = 4

    def method_c(self):
        ClassC.var1 = ClassC.var1 + ClassC.var2
        return ClassC.var1


class ClassD(ClassC):
    def __init__(self):
        ClassC.__init__(self)
        print("That is the second approach: ")
        print(ClassC.var1, ClassC.var2)


object1 = ClassC()

object2 = ClassD()

# ALTERNATIVELY YOU CAN USE:


class ClassE(object):
    def __init__(self):
        self.var1 = 5
        self.var2 = 10

    def method_e(self):
        self.var1 = self.var1 + self.var2
        return self.var1


class ClassF(ClassE):
    def __init__(self):
        super().__init__()
        print("var1 =", self.var1)
        print("var2 =", self.var2)


object3 = ClassF()
_sum = object3.method_e()
print(_sum)


"""



var1 and var2 is an Instance variables of ClassA. 
Create an Instance of ClassB and when calling the methodA it will check the methodA in Child class (ClassB) first, 
If methodA is not present in ClassB you need to invoke the ClassA by using the super() method which will get you all the methods implemented in ClassA.
Now, you can access all the methods and attributes of ClassB.

https://stackoverflow.com/questions/222877/what-does-super-do-in-python-difference-between-super-init-and-expl

https://stackoverflow.com/questions/222877/what-does-super-do-in-python-difference-between-super-init-and-expl


"""
"""
ANOTHER SOLUTION: 

#Class1
class Test:
    def __init__(self):
        self.a = 10
        self.b = 20
        self.add = 0

    def calc(self):
        self.add = self.a+self.b

#Class 2
class Test2:
    def display(self):
        print('adding of two numbers: ',self.add)
#creating object for Class1
obj = Test()
#invoking calc method()
obj.calc()
#passing class1 object to class2
Test2.display(obj)





"""
