"""
import <module_to_insert>

We can do:
tim = turtle.Turtle()
every single time we use Turtle Class

But it is better to do:
from turtle import Turtle()





from turtle import *
# This means import everything, but then you do not know why for example \
the method forward is available and to which class belongs to.
# SO it is recommended to use from turtle import Turtle()



Aliases: Giving the module the name you want. It is presented below

import turtle as t
tim = t.Turtle()




"""


# import heroes
#
# print(heroes.gen())
