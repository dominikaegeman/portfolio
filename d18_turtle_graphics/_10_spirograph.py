from turtle import Turtle, Screen

# home

# + counterclockwise direction
# - clockwise direction
# To draw a regular polygon with 5 sides: tim.circle(100,360,5)
# 100 is a size
# 360 is an angle
# 5 is amount of sides


tim = Turtle()
tim.shape("circle")
tim.fillcolor("")
tim.shapesize(10)


colours = ["CornflowerBlue", "DarkOrchid", "IndianRed", "DeepSkyBlue", "LightSeaGreen", "wheat", "SlateGray", "SeaGreen"]
counter = -1

for _ in range(60):
    counter += 1
    if counter > 7:
        counter = -1
    tim.speed(0)
    tim.penup()
    tim.circle(-100, 354, 100)
    tim.pendown()
    tim.pencolor(colours[counter])
    tim.stamp()


screen = Screen()
screen.exitonclick()
