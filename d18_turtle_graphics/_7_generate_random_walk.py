from turtle import Turtle, Screen
import random


def choosing_colour():
    colours = ["CornflowerBlue", "DarkOrchid", "IndianRed", "DeepSkyBlue", "LightSeaGreen",
               "wheat", "SlateGray", "SeaGreen"]
    colour = random.choice(colours)
    return colour


def choosing_direction():
    directions = ["forward", "backward", "right", "left"]
    direction = random.choice(directions)
    return direction


def start():
    screen = Screen()
    screen.bgcolor("light blue")

    line_segment = Turtle()
    line_segment.shape("square")
    line_segment.shapesize(0.2)
    line_segment.pensize(10)
    line_segment.speed(5)

    while True:
        colour = choosing_colour()
        direction = choosing_direction()
        if direction == "forward":
            line_segment.color(colour)
            line_segment.seth(90)
            line_segment.forward(50)
        elif direction == "right":
            line_segment.color(colour)
            line_segment.seth(0)
            line_segment.forward(50)
        elif direction == "left":
            line_segment.color(colour)
            line_segment.seth(180)
            line_segment.forward(50)
        elif direction == "backward":
            line_segment.color(colour)
            line_segment.seth(270)
            line_segment.forward(50)


start()
