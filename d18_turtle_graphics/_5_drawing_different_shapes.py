from turtle import Turtle, Screen

"""
tim = Turtle()
angle = 360 / num_sides
tim.forward(100)
tim.right(angle)

"""

screen = Screen()
screen.bgcolor("alice blue")


shape = Turtle()

for _ in range(3):
    shape.forward(100)
    shape.right(120)


shape.pencolor("deep sky blue")

for _ in range(4):
    shape.forward(100)
    shape.right(90)


shape.pencolor("dark cyan")

for _ in range(5):
    shape.forward(100)
    shape.left(288)


shape.pencolor("pink")

for _ in range(6):
    shape.forward(100)
    shape.left(300)


shape.pencolor("peru")
for _ in range(7):
    shape.forward(100)
    shape.left(308.6)

shape.pencolor("gold")
for _ in range(8):
    shape.forward(100)
    shape.left(315)


shape.pencolor("dark red")
for _ in range(9):
    shape.forward(100)
    shape.left(320)

shape.pencolor("royal blue")
for _ in range(10):
    shape.forward(100)
    shape.left(324)


screen.exitonclick()
