import turtle as t
import random

tim = t.Turtle()
t.colormode(255)


def random_color():
    # random.randint(3,9) Return a number between 3 and 9 (both included):
    r = random.randint(0, 255)
    g = random.randint(0, 255)
    b = random.randint(0, 255)
    chosen_colour = (r, g, b)
    return chosen_colour


directions = [0, 90, 180, 270]
tim.pensize(15)
tim.speed("fastest")


# 0 will be facing east
# 90 will be facing north
# 180 will be facing west
# 270 will be facing south


for _ in range(200):
    # chosen_colour = random_color()
    tim.color(random_color())
    tim.forward(30)
    tim.setheading(random.choice(directions))


"""
turtle.colormode(cmode=None) basically lets the programmer choose how they would like python to interpret the number passed into the brackets for the colors.

There are these options for cmode: 1 and 255.

For the 1 mode, the programmer can only use numbers between 0 and 1 to represent the rgb scale, otherwise, a TurtleGraphicsError will be raised.

For the 255 option, the programmer can use numbers between 0 and 255. When using the 1 option, the color

(0.33, 0.33, 0.33)
will be the equivalent of

(85, 85, 85) #(255*0.33, 255*0.33, 255*0.33)
when using the 255 mode.


"""