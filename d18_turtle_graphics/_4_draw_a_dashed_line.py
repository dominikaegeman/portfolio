
from turtle import Turtle, Screen


piece = Turtle()

# for _ in range(10):
#     piece.forward(10)
#     piece.color("white")
#     piece.forward(10)
#     piece.color("black")
# BUT THE SAME CAN BE DONE BY PENDOWN PENUP

piece.speed(1)

for _ in range(15):
    piece.forward(10)
    piece.penup()
    piece.forward(10)
    piece.pendown()


# print(piece.pen()) .pen to check some info


screen = Screen()
screen.exitonclick()
