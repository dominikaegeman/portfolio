import turtle as t


tim = t.Turtle()

colors = ["alice blue", "deep sky blue", "dark cyan", "pink", "peru", "gold", "dark red", "royal blue"]


def draw_shape(num_sides, color):
    angle = 360 / num_sides
    for _ in range(num_sides):
        tim.pencolor(color)
        tim.forward(100)
        tim.right(angle)


def choosing_colour(shape_side_n):
    num_colour = shape_side_n - 3
    colour = colors[num_colour]
    return colour


for shape_side_n in range(3, 11):
    chosen_colour = choosing_colour(shape_side_n)
    draw_shape(shape_side_n, chosen_colour)

# Interchangeable we can use tim.color(random.choice(colors)
