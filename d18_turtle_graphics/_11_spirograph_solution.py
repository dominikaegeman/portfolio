import turtle as t
import random
tim = t.Turtle
t.colormode(255)


def random_colour():
    r = random.randint(0, 255)
    g = random.randint(0, 255)
    b = random.randint(0, 255)
    colour = (r, g, b)
    return colour


t.speed(0)


def draw_spirograph(size_of_gap):
    for _ in range(int(360 / size_of_gap)):
        t.color(random_colour())
        t.circle(100)

        current_heading = t.heading()
        t.setheading(current_heading + size_of_gap)
        t.circle(100)
        print(t.heading())


draw_spirograph(5)

# draw_spirograph(5) and for _ in range(360 / size_of_gap) will give as an error that
# float is not acceptable and we need an integer
# TypeError: 'float' object cannot be interpreted as an integer
# in Python 360 / 5 = 72.0 so it is a float
# use // to receive an int
# or just transfer it into an int = int(360 / size_of_gap)

screen = t.Screen()
screen.exitonclick()

# ctrl d to duplicate the same line!
# print(tim.heading()) checking the heading
# set the heading = .setheading
