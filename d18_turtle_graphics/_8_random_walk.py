import turtle as t
import random

tim = t.Turtle()
colours = ["CornflowerBlue", "DarkOrchid", "IndianRed", "DeepSkyBlue", "LightSeaGreen",
           "wheat", "SlateGray", "SeaGreen"]
directions = [0, 90, 180, 270]


# 0 will be facing east
# 90 will be facing north
# 180 will be facing west
# 270 will be facing south

tim.pensize(15)
tim.speed("fastest")


for _ in range(200):
    tim.color(random.choice(colours))
    tim.forward(30)
    tim.setheading(random.choice(directions))

# Tuple is a data type in Python
# my_tuple = (1,3,8)
# It can store different types of data
# You cannot change the values in tuples
# my_tuple[2] = 12 # Not possible, it has been already set up as 8
# You cannot remove items from the tuple
# Tuple is immutable - it cannot be changed
# Tuple - you do not want sb to accidentally mess it up
# But if you really want to change tuple then put it into a list
# list(my_tuple)
