from turtle import Turtle, Screen

my_square = Turtle()
my_square.shape("turtle")
my_square.color("dark sea green")

for _ in range(4):
    my_square.forward(200)
    my_square.right(90)

# importing_modules_installing packages_working_with_aliases

# Another way of solving this:

# my_square.forward(200)
# my_square.right(90)
# my_square.forward(200)
# my_square.right(90)
# my_square.forward(200)
# my_square.right(90)
# my_square.forward(200)


screen = Screen()
screen.exitonclick()
