class Employee:

    # Initializing
    def __init__(self):
        print('Employee created.')

    # Deleting (Calling destructor)
    def __del__(self):
        print('Destructor called, Employee deleted.')


obj = Employee()
del obj


"""

Useful link: https://www.geeksforgeeks.org/python-__delete__-vs-__del__/#:~:text=for%20operator%20overloading.-,__del__,an%20object%20is%20garbage%20collected.&text=Example%3A%20Here%20is%20the%20simple,'%2C%20therefore%20destructor%20invoked%20automatically.

"""