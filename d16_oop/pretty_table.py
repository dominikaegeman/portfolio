# from prettytable import PrettyTable
# # Method is function associated with the object!
# # method table.add_column()
#
# table = PrettyTable()
#
# table.add_column("Pokemon Name",["Pikachu","Squirtle","Charmander"])
# table.add_column("Type",["Electric","Water","Fire"])
#
# table.align = "l"
# print(table)

# from prettytable import from_csv
# with open("Data_to_import.csv") as fp:
#     mytable = from_csv(fp)
#     print(mytable)
