import another_module
from turtle import Turtle, Screen

print(another_module.another_variable)

# import turtle
# tinny = turtle.Turtle()

tinny = Turtle()

print(tinny)
# How to call methods that are associated with the object
tinny.shape("turtle")
tinny.color("coral")
tinny.forward(100)


my_screen = Screen()
print(my_screen.canvheight)  # name of the attribute


my_screen.exitonclick()
