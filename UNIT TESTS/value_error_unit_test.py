
import unittest

class Order:
    def __init__(self):
        pass

    def making_an_order(self, order):    
        if order == 0:
            raise ValueError("Not possible")
        
        elif order == 5:
            return 5
            


class TestMakeAnOrder(unittest.TestCase):
    
    def test_make_an_order(self):
        order = Order()
        
        self.assertRaises(ValueError, lambda: order.making_an_order(0))
        self.assertEqual(order.making_an_order(5), 5)

        
        
if __name__ == "__main__":
    unittest.main()


