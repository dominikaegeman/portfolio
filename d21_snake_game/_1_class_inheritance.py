"""
Class Inheritance
Classes can inherit from other classes (attributes, methods)

You can inherit appearance and behaviour






Slicing (list, dictionaries)

"""


class Animal:
    def __init__(self):
        self.num_eyes = 2

    def breathe1(self):
        print("Inhale, exhale")


class Fish(Animal):
    def __init__(self):
        super().__init__()

    def breathe(self):
        super().breathe1()
        print("doing this underwater")

    def swim(self):
        print("moving in water")


nemo = Fish()
# nemo.swim()
nemo.breathe()
# print(nemo.num_eyes)



