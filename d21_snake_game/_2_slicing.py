"""
SLICING:
piano_keys = [a,b,c,d,e,f,g]
piano_keys[2:5]


"""
import os
from turtle import Turtle, Screen
import os

turtle = Turtle()
screen = Screen()
screen.addshape(os.path.expandvars("~/Desktop/100 DAYS OF PYTHON/slicing.gif"))
turtle.shape(os.path.expandvars("~/Desktop/100 DAYS OF PYTHON/slicing.gif"))
piano_keys = ["a", "b", "c", "d", "e", "f", "g"]
print(piano_keys[2:5])
# You will get all of the rest of the list
print(piano_keys[2:])
print(piano_keys[:5])
# You want from c (2) to e (including e) but printing every second letter
print(piano_keys[2:5:2])
# Go from the beginning to the end and skip every second one
print(piano_keys[::2])
# Going from the end to the beginning
print(piano_keys[::-1])
"""
Output: ['c', 'd', 'e']
['c', 'd', 'e', 'f', 'g']
['a', 'b', 'c', 'd', 'e']
['c', 'e']
['a', 'c', 'e', 'g']
['g', 'f', 'e', 'd', 'c', 'b', 'a']


"""


piano_tuple = ("do", "re", "mi", "fa", "so", "la", "ti")
print(piano_tuple[2:5])
screen.exitonclick()


ala = [0, 1, 2, 3, 4]
for number in ala[1:]:
    print(number)
