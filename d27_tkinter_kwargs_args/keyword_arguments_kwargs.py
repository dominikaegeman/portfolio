# We refer to the keyword arguments kwargs by a name
# UNLIMITED KEYWORD ARGUMENTS **kwargs


def calculate(n, **kwargs):
    print(type(kwargs)) # It will print a dictionary
    """{'add': 3, 'multiply': 5}
    """

    for key, value in kwargs.items():
        # print(key)
        # print(value)
        print(kwargs["add"]) # Output: 3
        n += kwargs["add"]
        n *= kwargs["multiply"]
        print(n)

calculate(2, add=3, multiply=5)


class Car():
    def __init__(self, **kwargs):
        self.make = kwargs["make"]
        self.model = kwargs["model"]
        


my_car = Car(make="Nissan", model="GT-R")
print(my_car.model)

my_car = Car(make="Nissan") # Error due to the lack of kwatgs["model"]


# To avoid an error, use get() function
class CarTwo():
    def __init__(self, **kwargs):
        self.make = kwargs.get("make")
        self.model = kwargs.get("model")

car_2 = CarTwo()