def add(n1, n2):
    return n1 + n2

add(n1=5, n2=2)

# But the better solution is

def add(*args):
    for n in args:
        print(n)
        
# The easterisk is very important one, it can accept any number of arguments
# args = form of a tuple


# Unlimited positional arguments below. We refer to the arguments by a position
def add_numbers(*args):
    print(args[0])
    the_sum = 0
    for number in args:
        the_sum += number
    print(the_sum)
    
add_numbers(2,4,5,22,4)