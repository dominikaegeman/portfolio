import random

# new_dict = {new_key:new_value for item in list/string/range}
names = ["Alex", "Beth", "Caroline", "Dave", "Eleanor", "Freddie"]
student_scores = {student:random.randint(1,100) for student in names}
print(student_scores)


# Creating a dictionary from an existing dictionary
# new_dict = {new_key:new_value for (kwy, value) in dict.items() if test}
passed_students = {student_name:student_score for (student_name, student_score) in student_scores.items() if student_score >= 60}
print(passed_students)


