sentence = "What is the Airspeed Velocity of an Unladen Swallow?"
# Don't change code above 👆

# Write your code below:

words_list = sentence.split()


result = {word:len(word) for word in words_list}



print(result)


d = {'key':'value'}
print(d)
# {'key': 'value'}
d['mynewkey'] = 'mynewvalue'
print(d)
# {'mynewkey': 'mynewvalue', 'key': 'value'}



dic ={"geeks": "A","for":"B","geeks":"C"}

value = {i for i in dic if dic[i]=="B"}
print("key by value:",value)



a = "sffd,fwe,tr,ewe,ew"
b = a.split(",")
print(b)

weather_c = {
    "Monday": 12,
    "Tuesday": 14,
    "Wednesday": 15,
    "Thursday": 14,
    "Friday": 21,
    "Saturday": 22,
    "Sunday": 24,
}
# 🚨 Don't change code above 👆


# Write your code 👇 below:




weather_f = {item_key : (item_value * 9/5) + 32 for item_key, item_value in weather_c.items()}

print(weather_f)


