# Unique for Python 

#  List comprehension: It is a case when you create a new list from an existed list

# So far it has been done by for loop:

numbers = [1,2,3]

new_list = []
for n in numbers:
    add_n = n + 1
    new_list.append(add_n)

# Using list comprehension: 
# template: new_list = [new_item for item in list]

new_list_using_list_comprehension = [n + 1 for n in numbers]
print(new_list_using_list_comprehension)
    
name = "Angela"

name_list = [letter for letter in name]
print(name_list)

range(1,5)

list_from_range = [number * 2 for number in range(1,5)]
print(list_from_range)



# Conditional list comprehension
# Template: new_list = [new_item for item in list if test]

names = ["Alex", "Beth", "Caroline", "Dave", "Eleanor", "Freddie"]

short_names = [name for name in names if len(name) < 5]
print(short_names)

upper_case_list = [name.upper() for name in names if len(name) > 5]
print(upper_case_list)



numbers = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]
# 🚨 Do Not Change the code above 👆

#Write your 1 line code 👇 below:

squared_numbers = [number ** 2 for number in numbers]

#Write your code 👆 above:

print(squared_numbers)

numbers = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]
# 🚨 Do Not Change the code above

#Write your 1 line code 👇 below:

result = [number for number in numbers if number % 2 == 0]


#Write your code 👆 above:

print(result)

with open("file1.txt", "r") as file1:
    file1_data = file1.readlines()

    file1_data_list = [int(number) for number in file1_data]




with open("file2.txt", "r") as file2:
    file2_data = file2.readlines()

    file2_data_list = [int(number) for number in file2_data]





result = [number for number in file1_data_list if number in file2_data_list]




# Write your code above 👆

print(result)


