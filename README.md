## Portfolio
Portfolio Project aims to emphasise my process of learning.

## Description
This Project also allows to highlight my soft skills including: ability to clearly communicate ideas to others, solving unexpected problems, research and planning.

## Installation
There is no installation required. 

## Support
![Discord](https://img.shields.io/discord/1035136078358777877?color=pink&style=plastic)


If you are seeking support, please visit our discord channel [HERE](https://discord.gg/S7xKXZGx4k)

## Roadmap
V1.0 will constitute a whole repository of projects.

## Authors and acknowledgment
Dominika

## License
[![MIT](https://img.shields.io/badge/license-MIT-green)](https://choosealicense.com/licenses/mit/)




